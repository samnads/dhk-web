<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        return view('home', $data);
    }
}
