<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionPackageController extends Controller
{
    public function packages(Request $request)
    {
        $data['api_data'] = customerApiCall('data', [])['result'];
        return view('packages.packages', $data);
    }
}
