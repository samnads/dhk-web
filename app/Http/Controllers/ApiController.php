<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Response;

class ApiController extends Controller
{
    public function customer_api_call(Request $request, $endpoint)
    {
        // to handle any ajax requests
        $data = [];
        /*************************************************************** */
        // customize data for real API
        foreach ($request->all() as $key => $value) {
            if ($key == "cleaning_materials") {
                $value = (boolean) $value; // convert int (1/0) to boolean
            }
            $data['params'][$key] = $value;
        }
        $response = customerApiCall($endpoint, $data, $request->method());
        /*************************************************************** */
        if (@$response['result']['status'] == 'success') {
            if ($endpoint == 'check_otp') {
                $session_data = [
                    'customer_id' => $response['result']['UserDetails']['id'],
                    'customer_name' => $response['result']['UserDetails']['UserName'],
                    'customer_token' => $response['result']['UserDetails']['token'],
                    'customer_avatar' => $response['result']['UserDetails']['image'],
                    'customer_mobile' => $response['result']['UserDetails']['mobile'],
                    'customer_email' => $response['result']['UserDetails']['email']
                ];
                session($session_data);
                if (isset($response['result']['UserDetails']['default_address_id'])) {
                    Session::put('customer_default_address_id', $response['result']['UserDetails']['token']);
                }
            } else if ($endpoint == 'update_customer_data') {
                Session::put('customer_token', $response['result']['UserDetails']['token']);
                Session::put('customer_name', $response['result']['UserDetails']['UserName']);
                Session::put('customer_email', $response['result']['UserDetails']['email']);
            } else if ($endpoint == 'name_entry') {
                Session::put('customer_token', $response['result']['UserDetails']['token']);
                Session::put('customer_name', $response['result']['UserDetails']['UserName']);
                Session::put('customer_email', $response['result']['UserDetails']['email']);
                Session::put('customer_mobile', $response['result']['UserDetails']['mobile']);
            } else if ($endpoint == 'update_avatar') {
                Session::put('customer_avatar', $response['result']['UserDetails']['image']);
            } else if ($endpoint == 'create_booking') {
                // save data from gateways
                if (@$response['result']['telr_data']['order']['ref']) {
                    Session::put('telr_order_ref', $response['result']['telr_data']['order']['ref']);
                    Session::put('telr_order_url', $response['result']['telr_data']['order']['url']);
                }
            } else if ($endpoint == 'retry_payment') {
                // save data from gateways
                if (@$response['result']['telr_data']['order']['ref']) {
                    Session::put('telr_order_ref', $response['result']['telr_data']['order']['ref']);
                    Session::put('telr_order_url', $response['result']['telr_data']['order']['url']);
                }
            } else if ($endpoint == 'invoice-payment') {
                // save data from gateways
                if (@$response['result']['telr_data']['order']['ref']) {
                    Session::put('telr_order_ref', $response['result']['telr_data']['order']['ref']);
                    Session::put('telr_order_url', $response['result']['telr_data']['order']['url']);
                }
            } else if ($endpoint == 'online-payment') {
                // save data from gateways
                if (@$response['result']['telr_data']['order']['ref']) {
                    Session::put('telr_order_ref', $response['result']['telr_data']['order']['ref']);
                    Session::put('telr_order_url', $response['result']['telr_data']['order']['url']);
                }
            }
        }
        /*************************************************************** */
        if ($endpoint == 'customer_logout') {
            Session::flush();
        }
        /*************************************************************** */
        return $response;
    }
    public function telr_api_call(Request $request, $endpoint)
    {
        $segments = explode('/', $endpoint);
        $data['params']['platform'] = 'web';
        if ($segments[0] == 'process') {
            /************************************************************************ */
            $booking_ref = $segments[1];
            $data['params']['booking_ref'] = $segments[1];
            $data['params']['booking_id'] = $segments[2];
            $data['params']['telr_order_ref'] = session('telr_order_ref');
            $data['params']['telr_order_url'] = session('telr_order_url');
            $endpoint = 'telr/process';
            $response = apiCall($endpoint, $data, 'POST');
            dd($response);
            //$card_type = $response['result']['telr_data']['order']['card']['type'];
            if ($response['result']['status'] == "success") {
                return redirect('booking/success/' . $booking_ref.'?reference_id=' . $booking_ref . '&payment_method=Card&status=Confirmed');
                //echo 'success';
            } else {
                return redirect('booking/failed/' . $booking_ref . '?reference_id='.$booking_ref.'&payment_method=Card&status=Declined');
            }
        }
        else if ($segments[0] == 'invoice-process') {
            /************************************************************************ */
            $payment_id = $segments[1];
            $data['params']['payment_id'] = $segments[1];
            $data['params']['telr_order_ref'] = session('telr_order_ref');
            $data['params']['telr_order_url'] = session('telr_order_url');
            $endpoint = 'telr/invoice-process';
            $response = apiCall($endpoint, $data, 'POST');
            //dd($response);
            if ($response['result']['status'] == "success") {
                return redirect('invoice-payment/success/' . $payment_id);
                //echo 'success';
            } else {
                return redirect('invoice-payment/failed/' . $payment_id);
            }
        } else if ($segments[0] == 'online-payment-process') {
            /************************************************************************ */
            $payment_id = $segments[1];
            $data['params']['payment_id'] = $segments[1];
            $data['params']['telr_order_ref'] = session('telr_order_ref');
            $data['params']['telr_order_url'] = session('telr_order_url');
            $endpoint = 'telr/online-payment-process';
            $response = apiCall($endpoint, $data, 'POST');
            //dd($response);
            if ($response['result']['status'] == "success") {
                return redirect('online-payment/success/' . $payment_id);
                //echo 'success';
            } else {
                return redirect('online-payment/failed/' . $payment_id);
            }
        }
        //return Response::json($response, 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    public function hijack_customer_login(Request $request)
    {
        $params['params']['id'] = $request->id;
        $params['params']['token'] = $request->token;
        $response = customerApiCall('validate_token', $params);
        if (@$response['result']['status'] == "success") {
            $session_data = [
                'customer_id' => $response['result']['customer']['customer_id'],
                'customer_name' => $response['result']['customer']['customer_name'],
                'customer_token' => $response['result']['customer']['oauth_token'],
                'customer_avatar' => $response['result']['customer']['customer_photo_file'],
                'customer_mobile' => $response['result']['customer']['mobile_number_1'],
                'customer_email' => $response['result']['customer']['email_address']
            ];
            session($session_data);
            return 'Login session saved successfully !';
        } else {
            return 'Expired Token or Invalid Customer !';
        }
    }
}
