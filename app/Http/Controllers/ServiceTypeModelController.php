<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ServiceTypeModelController extends Controller
{
    public function normal_service(Request $request, $data)
    {
        $coupon_code = $request->segment(2);
        if($coupon_code){
            Cookie::queue(Cookie::make('coupon_code', $coupon_code));
        }
        $data['api_data'] = customerApiCall('data', [])['result'];
        /*********************************************************************** */
        $params = [];
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data['api_service_type_data'] = customerApiCall('service_type_data', $params)['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        /*********************************************************************** */
        return view('service-model.normal', $data);
    }
    public function package_service(Request $request, $data)
    {
        $coupon_code = $request->segment(2);
        if ($coupon_code) {
            Cookie::queue(Cookie::make('coupon_code', $coupon_code));
        }
        $data['api_data'] = customerApiCall('data', [])['result'];
        /*********************************************************************** */
        // get packages data
        $params = [];
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data['api_service_type_data'] = customerApiCall('service_type_data', $params)['result'];
        $data['api_package_data'] = customerApiCall('packages', $params)['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        /*********************************************************************** */
        return view('service-model.package', $data);
    }
    public function custom_service(Request $request, $data)
    {
        $coupon_code = $request->segment(2);
        if($coupon_code){
            Cookie::queue(Cookie::make('coupon_code', $coupon_code));
        }
        $data['api_data'] = customerApiCall('data', [])['result'];
        /*********************************************************************** */
        $params = [];
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data['api_service_type_data'] = customerApiCall('service_type_data', $params)['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        /*********************************************************************** */
        return view('service-model.custom', $data);
    }
    public function subscription_package(Request $request, $data)
    {
        if (!$request->step) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        } else if (@$request->step > 1) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        }
        $data['api_data'] = customerApiCall('data', [])['result'];
        /*********************************************************************** */
        // get packages data
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        /*********************************************************************** */
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data['api_service_type_data'] = customerApiCall('service_type_data', $params)['result'];
        return view('service-model.subs-package', $data);
    }
    public function enquiry_service(Request $request, $data)
    {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $params = [];
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data['api_package_data'] = customerApiCall('packages', $params)['result'];
        $data['api_service_type_data'] = customerApiCall('service_type_data', $params)['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        return view('service-model.enquiry', $data);
    }
}
