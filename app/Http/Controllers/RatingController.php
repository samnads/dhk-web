<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\RatingConfirmationMail;

class RatingController extends Controller
{
    public function rating($date,$serviceId,$bookingId)
    {
        $data = [$date,$serviceId,$bookingId];
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('url.api_url').'get-rate-review', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'date'=>$date,
                'day_service_id'=>$serviceId,
                'booking_id'=>$bookingId,
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['data']['day_service']['rating'] > 0){
            return 'Rating Already Submitted !';
        }
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
           $rate = $responseBody['data']['rating'];
           $review = $responseBody['data']['comments'];
        } else {
            $rate = 0;
            $review = '';
        }
        return view('ratings.rating',['date'=>$date,'serviceId'=>$serviceId,'bookingId'=>$bookingId,'rate'=>$rate,'review'=>$review]);
    }
    public function submitRate(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('url.api_url').'submit-rate', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'date'=>$request['date'],
                'day_service_id'=>$request['day_service_id'],
                'booking_id'=>$request['booking_id'],
                'ratingValue'=>$request['ratingValue'],
                'review'=>$request['review'],
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        return response()->json(
            $responseBody
        );
    }
}
