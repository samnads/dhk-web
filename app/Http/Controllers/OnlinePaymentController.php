<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Session;

class OnlinePaymentController extends Controller
{
    /*public function online_payment_entry(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('url.api_url') . 'customer/get-customer-odoo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => $data['id'],
            ],

        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $data['customer_odoo'] = $responseBody['data'];
        return view('invoice.invoice-payment-entry', $data);
    }*/
    public function online_payment_link_entry(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['customerId'] = $request->id;
        $data['amount'] = $request->amount;
        $data['description'] = $request->message;
        return view('online-payment.online-payment-link-entry', $data);
    }
    public function online_payment_link_save(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('url.api_url') . 'customer/save-online-pay', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => $request->all()
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $data['save_invoice_pay'] = $responseBody['data'];
        return view('online-payment.online-payment-checkout', $data);
    }
    public function online_payment_success($reference_id){
        $data['api_data'] = customerApiCall('data', [])['result'];
        $params['params']['reference_id'] = $reference_id;
        $online_payment_by_id = customerApiCall('online-payment-by-ref', $params)['result'];
        $data['online_payment'] = @$online_payment_by_id['online_payment'];
        $data['customer'] = @$online_payment_by_id['customer'];
        $data['customer_address'] = @$online_payment_by_id['customer_address'];
        //dd($data['online-payment']);
        if(strtolower($data['online_payment']['payment_status']) != 'success'){
            return view('online-payment.online-payment-failed', $data);
        }
        return view('online-payment.online-payment-success', $data);
    }
    public function online_payment_failed($reference_id)
    {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $params['params']['reference_id'] = $reference_id;
        $online_payment_by_id = customerApiCall('online-payment-by-ref', $params)['result'];
        $data['online_payment'] = @$online_payment_by_id['online_payment'];
        $data['customer'] = @$online_payment_by_id['customer'];
        $data['customer_address'] = @$online_payment_by_id['customer_address'];
        //dd($data['online-payment']);
        return view('online-payment.online-payment-failed', $data);
    }
}
