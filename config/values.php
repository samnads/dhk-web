<?php

return [
    'mail_name' => env('MAIL_FROM_NAME'),
    'feedback_to' => env('MAIL_FEEDBACK_TO'),
    'vat_amount' => env('VAT_AMOUNT'),
    'cleaning_amount' => env('CLEANING_AMOUNT'),
    'to_mail' => env('MAIL_TO'),
    'mail_admin' => env('MAIL_ADMIN'),
    'mail_bcc' => env('MAIL_BCC'),
    'checkout_success' => env('CHECKOUT_SUCCESS'),
    'checkout_fail' => env('CHECKOUT_FAIL'),
    'checkout_online_success' => env('CHECKOUT_ONLINE_SUCCESS'),
    'checkout_online_fail' => env('CHECKOUT_ONLINE_FAIL'),
    'checkout_invoice_success' => env('CHECKOUT_INVOICE_SUCCESS'),
    'checkout_invoice_fail' => env('CHECKOUT_INVOICE_FAIL'),
    'checkout_secret_key' => env('CHECKOUT_SECRET_KEY'),
    'checkout_primary_key' => env('CHECKOUT_PRIMARY_KEY'),
    'checkout_service_url' => env('CHECKOUT_SERVICE_URL'), 'checkout_token_url' => env('CHECKOUT_TOKEN_URL'), 'checkout_channel_id' => env('CHECKOUT_CHANNEL_ID'),
    'odoo_url' => env('ODOO_URL'), 'tamara_success' => env('TAMARA_SUCCESS'), 'tamara_fail' => env('TAMARA_FAIL'), 'tamara_hook' => env('TAMARA_HOOK'), 'tamara_secret_key' => env('TAMARA_SECRET_KEY'), 'tamara_checkout_url' => env('TAMARA_CHECKOUT_URL'), 'tamara_payment_type_url' => env('TAMARA_PAYMENT_TYPE_URL'), 'tamara_precheck_url' => env('TAMARA_PRECHECK_URL'), 'apple_merchant_id' => env('APPLE_MERCHANT_ID'),
    'checkout_gpay_invoice_success' => env('CHECKOUT_GPAY_INVOICE_SUCCESS'),
    'checkout_gpay_invoice_fail' => env('CHECKOUT_GPAY_INVOICE_FAIL'),
    'default_service_id' => env('DEFAULT_SERVICE_ID'),
    'vat_percentage' => env('VAT_PERCENTAGE'),
    'pay_by_cash_charge' => env('PAY_BY_CASH_CHARGE'),
    'ccavenue_merchant_id' => env('CCAVENUE_MERCHANT_ID'),
    'ccavenue_workingkey' => env('CCAVENUE_WORKINGKEY')
];
