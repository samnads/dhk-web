@extends('layouts.main')
@section('title', 'My Account')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 top-steps-section">
                    <div class="d-flex page-title-section">
                        <div class="booking-page-title flex-grow-1">
                            <h3>Personal Details</h3>
                        </div>
                        <div class="step-back-icon"><a href="{{ url('profile') }}" class="back-arrow"
                                title="Click to Back">Back</a></div>
                    </div>
                </div>

                <div class="col-sm-12 my-account-wrapper">
                    <div class="row m-0 pt-1">
                        <div class="col-sm-6 n-personal-details m-auto">
                            <form id="profile_update" novalidate="novalidate">
                                <div class="col-sm-12 personal-details-section">
                                    <!-- <div class="pd-content-main flex-grow-1">
                                                        <div class="alert alert-danger">Hai</div>
                                                    </div> -->
                                    <div class="d-flex personal-details-main">
                                        <div class="pd-icon-main pd-name-icon v-center"><img
                                                src="{{ asset('images/user-icon7.png') }}" alt="" /></div>
                                        <div class="pd-content-main flex-grow-1">
                                            <p>Full Name</p>
                                            <input name="name" id="profileName" placeholder="" class="text-field"
                                                value="{{ session('customer_name') }}" type="text" autocomplete="off">
                                        </div>
                                        <!--<div class="pd-edit-main d-flex align-items-end edit-profile-name-btn">
                                                            <label><i class="fa fa-pencil"></i></label>
                                                        </div>-->
                                    </div>
                                </div>

                                <div class="col-sm-12 personal-details-section">
                                    <div class="d-flex personal-details-main edit-details">
                                        <div class="pd-icon-main pd-phone-icon v-center"><img
                                                src="{{ asset('images/user-icon8.png') }}" alt="" /></div>
                                        <div class="pd-content-main flex-grow-1">
                                            <p>Mobile</p>
                                            <input name="mobilenumber" id="profileMobile" placeholder="" class="text-field"
                                                value="{{ session('customer_mobile') }}" type="number" autocomplete="off">
                                        </div>
                                        <!--<div class="pd-edit-main d-flex align-items-end edit-profile-mobile-btn">
                                                            <label><i class="fa fa-pencil"></i></label>
                                                        </div>-->
                                    </div>
                                </div>

                                <div class="col-sm-12 personal-details-section border-0">
                                    <div class="d-flex personal-details-main">
                                        <div class="pd-icon-main  pd-email-icon v-center"><img
                                                src="{{ asset('images/user-icon9.png') }}" alt="" /></div>
                                        <div class="pd-content-main flex-grow-1">
                                            <p>Email</p>
                                            <input name="email" id="profileEmail" placeholder="" class="text-field"
                                                value="{{ session('customer_email') }}" type="email">
                                        </div>
                                        <!--<div class="pd-edit-main d-flex align-items-end edit-profile-email-btn">
                                                            <label><i class="fa fa-pencil"></i></label>
                                                        </div>-->
                                    </div>
                                </div>

                                <div class="col-sm-12 booking-main-btn-section p-0 pt-4">
                                    <div class="row m-0">
                                        <div class="col-lg-5 col-md-4 col-sm-6 p-0 pt-3">
                                            <button class="text-field-btn" id="profile-update-submit" type="submit"
                                                style="margin-left: 50px;">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.edit-profile-name-btn').click(function() {
                $("#profileName").attr("readonly", false);
            });
            $('.edit-profile-mobile-btn').click(function() {
                $("#profileMobile").attr("readonly", false);
            });
            $('.edit-profile-email-btn').click(function() {
                $("#profileEmail").attr("readonly", false);
            });

            login_form_validator = $('#profile_update').validate({
                focusInvalid: false,
                ignore: [],
                errorElement: 'p',
                errorClass: "text-danger",
                rules: {
                    "name": {
                        required: true,
                    },
                    "mobilenumber": {
                        required: true,
                        minlength: 9,
                        maxlength: 9
                    },
                    "email": {
                        required: true,
                    }
                },
                messages: {
                    "name": {
                        required: "Enter your name",
                    },
                    "mobilenumber": {
                        required: "Enter your mobile number",
                    },
                    "email": {
                        required: "Enter your email address",
                    }
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element.append());
                },
                submitHandler: function(form) {
                    let submit_btn = $('button[type="submit"]', form);
                    submit_btn.html(loading_button_html).prop("disabled", true);
                    $.ajax({
                        type: 'POST',
                        url: _base_url + "api/customer/update_customer_data",
                        dataType: 'json',
                        data: $('#profile_update').serialize(),
                        success: function(response) {
                            submit_btn.html('Update').prop("disabled", false);
                            if (response.result.status == "success") {
                                if (response.result.check_otp == true) {
                                    // customer trying to change mobile
                                    // we have already send otp
                                    toast('OTP Sent', response.result.message,
                                        'info');
                                    $('#login-otp-popup-form input[name="mobilenumber"]')
                                        .val(response.result.UserDetails.current_mobile);
                                    $('#login-otp-popup-form input[name="new_mobilenumber"]')
                                        .val(response.result.UserDetails.mobile);
                                    //
                                    //let oldnum = $("#oldmobilenumber").val();
                                    // $('.login-popup').hide(500);
                                    //$('#login-otp-popup-form input[name="mobilenumber"]').val(oldnum);
                                    //$('#login-otp-popup-form input[name="oldmobilenumber"]').val(response.result.UserDetails.mobile);
                                    //$('#login-otp-popup-form input[name="is_update"]').val(1);
                                    $('.customer-full-mobile').html(response.result
                                        .UserDetails.mobile)
                                    showOtp();
                                } else {
                                    toast('Success', response.result.message, 'success');
                                }
                            } else {
                                toast('Error', response.result.message, 'error');
                            }
                        },
                        error: function(response) {
                            submit_btn.html('Update').prop("disabled", false);
                        },
                    });
                }
            });
        });
    </script>
@endpush
