<div class="popup-main otp-popup" id="otp-popup">
    <form id="login-otp-popup-form">
        <input name="country_code" type="hidden" value="971">
        <!--<input name="is_update" id="is_update" type="hidden" value="0">-->
        <!--<input name="oldmobilenumber" id="oldmobilenumber" type="hidden">-->
        <input name="id" type="hidden">
        <input name="mobilenumber" type="hidden">
        <input name="new_mobilenumber" type="hidden">
        <input name="type" type="hidden">
        <input name="email" type="hidden">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Verify phone number</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 otp-content">
                        <p><strong>Enter the code that was sent to</strong></p>
                        <h5 class="customer-full-mobile">+00-000000000</h5>
                    </div>
                    <div class="col-sm-12 otp-field p-0 border mt-3" style="display: none">
                        <input name="otp" type="number" pattern="/^-?\d+\.?\d*$/"
                            onKeyPress="if(this.value.length==4) return false;" class="text-field no-arrow" inputmode="numeric"/>
                    </div>

                    <div class="col-sm-12 otp-field-section p-0">
                        <div class="row otp-field-main">
                            <div class="col-3 otp-field p-0"><input placeholder="-" maxlength="1" name="otp-1"
                                    data-box="1" type="number" class="text-field bor-right no-arrow"></div>
                            <div class="col-3 otp-field p-0"><input placeholder="-" maxlength="1" name="otp-2"
                                    data-box="2" type="number" class="text-field bor-right no-arrow"></div>
                            <div class="col-3 otp-field p-0"><input placeholder="-" maxlength="1" name="otp-3"
                                    data-box="3" type="number" class="text-field bor-right no-arrow"></div>
                            <div class="col-3 otp-field p-0"><input placeholder="-" maxlength="1" name="otp-4"
                                    data-box="4" type="number" class="text-field no-arrow"></div>
                        </div>
                    </div>

                    <div class="col-sm-12 otp-field p-0">
                        <p><strong>Didn't receive a OTP ?</strong></p>
                        <a id="resend-otp" href="#" class="pull-left" data-action="resend-otp"
                            style="display: none">Resend
                            OTP</a>
                        <p id="resend-otp-timer" class="pull-left"></p>
                    </div>
                    <div class="col-sm-12 frequency-main pt-3">
                        <button class="text-field-btn" type="submit">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- OTP Popup-->