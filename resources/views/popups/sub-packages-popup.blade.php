<div class="popup-main" id="sub-packages-popup">
    <div class="row min-vh-100 m-0">
        <div class=" mx-auto my-auto shadow popup-main-cont">
            <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}" alt="">
            </div>
            <div class="col-sm-12 popup-head-text">
                <h4><span class="booking_id"></span></h4>
            </div>
            <div class="row m-0 mt-3">
                <div class="col-sm-12 calender-wrapper">
                    <div class="col-sm-12 p-0">
                        <h4><span class="service-name"></span>&nbsp;Packages</h4>
                    </div>
                    <div class="col-sm-12 calendar-main p-0">
                        <div id="calendar" class="owl-carousel owl-theme p-0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 what-time-wrapper">
                <div class="row">
                    <div class="row row-cols-1 row-cols-md-3 g-4" id="sub-packages-holder">
                        @foreach ($api_data['subscription_packages'] as $subscription_package)
                            <div class="col item" data-service_type_id="{{ $subscription_package['service_type_id'] }}">
                                <div class="card">
                                    <img src="{{$subscription_package['banner_image_url'] }}" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$subscription_package['package_name'] }}</h5>
                                        <p class="card-text">AED {{$subscription_package['amount'] }}</p>
                                        <button data-action="book-sub-package" data-id="{{ $subscription_package['package_id'] }}" class="btn btn-sm btn-primary pull-right mb-3" style="background-color: #195880;">Book Now</button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Sub Packages Popup-->
