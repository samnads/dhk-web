<div class="popup-main address-list-popup" id="change-pay-mode-popup">
    <form id="change-pay-mode-popup-form">
        <input name="booking_id" type="hidden" />
        <input name="PaymentMethod" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Change Payment Mode<span class="booking_id"></span></h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 payment-method-wrapper p-0">
                        <div class="col-sm-12 booking-form-list payment-method p-0">
                            <ul id="payment-method-holder">
                                @foreach ($api_data['payment_types'] as $key => $payment_type)
                                    @if ($payment_type['show_in_web'] == 1)
                                        <li class="pay-mode-li-{{ $payment_type['id'] }}"
                                            id="payment-mode-{{ $payment_type['id'] }}">
                                            <input id="payment-method-{{ $payment_type['id'] }}"
                                                value="{{ $payment_type['id'] }}" name="payment_method" class=""
                                                type="radio" {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                            <label for="payment-method-{{ $payment_type['id'] }}">
                                                <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                                <img
                                                    src="{{ asset('images/payment-' . $payment_type['id'] . '.jpg') }}" />
                                            </label>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 p-0 m-auto" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0" id="change-btn-pay-mode-1">
                                    <button class="text-field-btn" type="submit">Pay Now</button>
                                </div>
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0" id="change-btn-pay-mode-2">
                                    <button class="text-field-btn" type="submit">Pay Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
