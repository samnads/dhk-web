<div class="popup-main login-popup" id="required-popup">
    <form id="required-popup-form" novalidate="novalidate">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont overflow-visible">
                <div class="popup-close" data-action="close" style="display: none;"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Enter Details</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 login-content p-0">
                        <p>Please fill the required details to continue.</p>
                    </div>
                    <div class="col-sm-12 p-0">
                        <label>Name</label>
                        <input name="name" class="text-field" type="text">
                    </div>
                    <div class="col-sm-12 p-0 mt-2 email">
                        <label>Email</label>
                        <input name="email" class="text-field" type="email">
                    </div>
                    <div class="col-sm-12 p-0 mt-2 mobile">
                        <label>Mobile</label>
                        <input name="country_code" type="hidden" value="971">
                        <input name="mobilenumber" class="text-field" type="tel" style="width: 100%">
                    </div>
                    <div class="col-sm-12 frequency-main pt-3">
                        <button class="text-field-btn"
                            type="submit">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Login Popup-->