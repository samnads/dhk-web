<div class="popup-main frequency-popup" id="frequency-popup">
    <div class="row min-vh-100 m-0">
        <div class="mx-auto my-auto shadow popup-main-cont">
            <div class="popup-close"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
            <div class="col-sm-12 popup-head-text">
                <h4>Choose Your Frequency</h4>
            </div>
            <div class="row m-0">
                @foreach ($api_service_type_data['data']['frequency_list'] as $key => $frequency)
                    <div class="col-sm-12 frequency-main weekly">
                        <!--<div class="frequency-label">Most popular</div>-->
                        <input data-coupon_code="{{ $frequency['coupon_code'] }}" id="frequency{{ $frequency['code'] }}"
                            value="{{ $frequency['code'] }}" name="frequency" class="" type="radio"
                            {{ $frequency['code'] == 'OD' ? 'checked' : '' }}>
                        <label for="frequency{{ $frequency['code'] }}"> <span></span>{{ $frequency['name'] }}
                            @if ($frequency['percentage'] != null)
                                <strong>{{ $frequency['percentage'] }}% OFF</strong>
                            @endif
                            <div style="width: 100%; height:7px;">&nbsp;</div>
                            <p>{{ $frequency['description'] }}</p>
                        </label>
                        <!--<div class="frequency-offer">10%<span>OFF</span></div>-->
                    </div>
                @endforeach
                <div class="col-sm-6 frequency-main pt-3 border-0">
                    <input value="Select Frequency" class="text-field-btn" type="button" data-action="frequency-select">
                </div>
            </div>
        </div>
    </div>
</div><!-- Frequency Popup-->
