<div class="popup-main address-list-popup" id="telr-iframe-popup">
    <form id="reschedule-popup-form">
        <input name="booking_id" type="hidden" />
        <input name="hours" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4><span class="booking_id"></span></h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 calender-wrapper">
                        <iframe id="telr-iframe" src=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>