<div class="popup-main promocode-popup" id="coupon-apply-popup">
    <form id="coupon-apply-form">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Voucher Code</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 promo-code-field">
                        <input name="coupon_code" placeholder="Enter Promo Code" class="text-field" type="text">
                    </div>
                    <div class="col-sm-6 frequency-main pt-3">
                        <button class="text-field-btn" type="submit">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Promo Code Popup-->
