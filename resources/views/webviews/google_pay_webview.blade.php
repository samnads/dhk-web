@extends('layouts.webview')
@section('title', 'Google Pay')
@section('content')
    <!-- vh-100 here-->
    <div class="d-flex align-items-center justify-content-center vh-100">
        <div id="google-pay-button"></div>
    </div>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/pay.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/webview.google-pay.js?v=' . time()) }}"></script>
@endpush
