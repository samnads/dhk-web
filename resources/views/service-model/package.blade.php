@extends('layouts.main', ['body_css_class' => 'booking-section-page'])
@section('title', $service_type_name)
@section('content')
    <section>
        <div class="container">
            <form id="booking-form" name="booking-form" autocomplete="off" accept-charset="utf-8">
                <input name="id" value="{{ session('customer_id') }}" type="hidden">
                <input name="address_id" value="{{ session('customer_default_address_id') }}" type="hidden">
                <input name="token" value="{{ session('customer_token') }}" type="hidden">
                <input name="service_type_id" value="{{ $service_type_id }}" type="hidden">
                <input name="hours" type="hidden">
                <div class="row inner-wrapper m-0">
                    @include('includes.package.step-1')
                    @include('includes.package.step-2')
                    @include('includes.package.step-3')
                    @include('includes.package.summary')
                    @include('includes.package.step-next-buttons')
                </div>
            </form>
            @include('popups.coupon-apply-popup')
            @include('popups.address-list-popup')
            @include('includes.ccavenue-hidden-form')
        </div>
        @include('popups.package-details-popup')
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery.query-object.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.jscroll.js') }}"></script>
    <script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
    <script type="text/javascript" src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>
    <script type="text/javascript" src="{{ asset('js/pay.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.js?v=' . Config::get('version.js')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.package.js?v=' . Config::get('version.js')) }}"></script>
@endpush
