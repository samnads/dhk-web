@extends('layouts.app')
@section('title', 'Rating')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

<section class="em-booking-content-section rating-section">
    <div class=" em-booking-content-box"><!--container-->
        <div class="row min-vh-80 d-flex flex-column justify-content-center ml-0 mr-0">
        
            <div class="em-rating-set mx-auto shadow pl-0 pr-0">

                <div class="col-sm-12 pl-0 pr-0">
                    <div class="col-sm-12 my-account-content-main">
                        <input type="hidden" id="date" value="{{@$date}}">
                        <input type="hidden" id="day_service_id"  value="{{@$serviceId}}">
                        <input type="hidden" id="booking_id" value="{{@$bookingId}}">
                        <input type="hidden" id="ratingValue">
                        

                              <div class="col-sm-12 p-0 pt-3 pb-5">
                              
                                  <h5>Please rate your experience</h5>
                                  
                                  <div class="col-sm-12 em-field-main-set p-0"> 

                                          <div class="col-sm-12 em-field-main p-0">
                                              <!--<p class="pt-0">Enter Your Rate</p>-->
                                              <section class="rating-widget col-sm-12 my-account-content-main pl-0 pr-0" style="border: 0px;"> 
                                              <div class='rating-stars text-center'>
                                                  <ul id='stars' style="text-align: left; margin-left: -6px;">
                                                      <li @if($rate >0) class='star selected'  @else class='star' @endif 'title='Poor'  data-value='1'>
                                                          <i class='fa fa-star fa-fw'></i>
                                                      </li>
                                                      <li @if($rate >1) class='star selected'  @else class='star' @endif title='Fair' data-value='2'>
                                                          <i class='fa fa-star fa-fw'></i>
                                                      </li>
                                                      <li @if($rate >2) class='star selected'  @else class='star' @endif title='Good' data-value='3'>
                                                          <i class='fa fa-star fa-fw'></i>
                                                      </li>
                                                      <li @if($rate >3) class='star selected'  @else class='star' @endif title='Excellent' data-value='4'>
                                                          <i class='fa fa-star fa-fw'></i>
                                                      </li>
                                                      <li @if($rate >4) class='star selected'  @else class='star' @endif title='WOW!!!' data-value='5'>
                                                          <i class='fa fa-star fa-fw'></i>
                                                      </li>
                                                  </ul>
                                              </div>
                                          </section>
                                          </div>
                                          
                                          
                                          
                                          <div class="col-sm-12 em-field-main p-0 pt-1">
                                              <p>Your feedback</p>
                                              <div class="col-12 em-text-field-main pl-0 pr-0">
                                                  <textarea id="review" cols="" rows="" class="text-field-big" @if(@$review !='') disabled @endif>{{@$review}}</textarea>
                                              </div>
                                              <span  style="color:red;display:none;" id="review_error">Please enter your review.</span>
                                          </div>
                                  </div>
                              </div>
                              
       
                    </div>
                    <div class="col-12 em-next-btn pb-4">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-12 em-next-btn-right pl-0 pr-0">
                                <button class="text-field-button show6-step" id="submit_rate_button" style="width: 100%;">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!--<div class="col-sm-12 em-bottom-space">&nbsp;</div>page bottom white space-->
</section>



<div class="col-sm-12 popup-main login-popup" id="success-popup">
  <div class="row min-vh-100 d-flex flex-column justify-content-center">
    <div class="col-lg-4 col-md-7 col-sm-4 popup-content em-booking-det-cont mx-auto signin-set-main shadow">
      <h5 class=""><span class="em-login-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
      <span id="error_message_p" style="display:none;"></span> 
      <div class="row ml-0 mr-0">
        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
          We're thrilled you loved our service! Share this experience on Google on next screen? It helps others make informed choice.
        </div> 
      </div>         
    </div>
  </div>
</div>
<div class="col-sm-12 popup-main login-popup" id="error-popup">
  <div class="row min-vh-100 d-flex flex-column justify-content-center">
    <div class="col-lg-4 col-md-7 col-sm-4 popup-content em-booking-det-cont mx-auto signin-set-main shadow">
      <h5 class=""><span class="em-login-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
      <span id="error_message_p" style="display:none;"></span> 
      <div class="row ml-0 mr-0">
        <div class="col-lg-12 col-md-12 col-sm-12 text-center text-danger">
          Feedback submission failed, please try again
        </div> 
      </div>         
    </div>
  </div>
</div>



<div class="col-sm-12 popup-main forgot-password-popup" id="rating_popup">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
      <div class="col-md-4 popup-content em-booking-det-cont mx-auto otp-set-main">
        <h5 class="remove-btn"><span ><img src="{{asset('images/el-close-black.png')}}"
  title="" style=""></span></h5>
            <div class="col-md-12 text-center popup-content  mx-auto otp-set-main">
                <span id="rating_message"></span> 
            </div>
      </div>
     </div>
</div><!--popup-main end-->




@endsection

@push('scripts')
<style>
body {
  font-family:"Open Sans", Helvetica, Arial, sans-serif;
  color:#555;
  /*max-width:680px;*/
  margin:0 auto;
  padding:0 0px;
}

* {
  -webkit-box-sizing:border-box;
  -moz-box-sizing:border-box;
  box-sizing:border-box;
}

*:before, *:after {
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
}

.clearfix {
  clear:both;
}

.text-center {text-align:center;}

a {
  /*color: tomato;*/
  text-decoration: none;
}

a:hover {
  color: #2196f3;
}

pre {
display: block;
padding: 9.5px;
margin: 0 0 10px;
font-size: 13px;
line-height: 1.42857143;
color: #333;
word-break: break-all;
word-wrap: break-word;
background-color: #F5F5F5;
border: 1px solid #CCC;
border-radius: 4px;
}

.header {
  padding:20px 0;
  position:relative;
  margin-bottom:10px;
  
}

.header:after {
  content:"";
  display:block;
  height:1px;
  background:#eee;
  position:absolute; 
  left:30%; right:30%;
}

.header h2 {
  font-size:3em;
  font-weight:300;
  margin-bottom:0.2em;
}

.header p {
  font-size:14px;
}



#a-footer {
  margin: 20px 0;
}

.new-react-version {
  padding: 20px 20px;
  border: 1px solid #eee;
  border-radius: 20px;
  box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);
  
  text-align: center;
  font-size: 14px;
  line-height: 1.7;
}

.new-react-version .react-svg-logo {
  text-align: center;
  max-width: 60px;
  margin: 20px auto;
  margin-top: 0;
}





.success-box {
  margin:50px 0;
  padding:10px 10px;
  border:1px solid #eee;
  background:#f9f9f9;
}

.success-box img {
  margin-right:10px;
  display:inline-block;
  vertical-align:top;
}

.success-box > div {
  vertical-align:top;
  display:inline-block;
  color:#888;
}



/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}

#stars li {
  border-bottom: 0px;
  cursor: pointer;
}

.min-vh-90 { height: 90vh !important;}
.min-vh-80 { height: 80vh !important;}
.min-vh-70 { height: 70vh !important;}

.em-rating-set { width: 350px;}

.rating-section .my-account-content-main li { padding-top: 5px;}

@media (max-width: 991.98px) and (min-width: 320px) {
.my-account-content-main {
    margin-bottom: 0px;
}
}
@media (max-width: 475.98px) and (min-width: 320px) {
.my-account-content-main {
    margin-bottom: 0px;
}
.em-rating-set { width: 90%;}
}
.text-field-button {
    width: 100%;
    height: auto;
    border: 0px;
    cursor: pointer;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 13px;
    color: #555;
    line-height: 20px;
    font-weight: bold;
    text-align: center;
    text-transform: uppercase;
    background: #4bcdcb;
    padding: 10px 15px;
    border-radius: 5px;
}
.em-booking-content-section {
    padding-top: 50px;
    min-height: 100vh;
}

</style>
<script>
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(document).ready(function(){
  $('#success-popup .em-login-close-btn').on('click', function() {
    if($('#ratingValue').val() == 5){
      // open google review url
      window.open("https://goo.gl/2cY7Vc","_self");
    }
    else{
      window.open("https://booking.dubaihousekeeping.com","_self")
    }
  });    
  $('#error-popup .em-login-close-btn').on('click', function() {
   $('#error-popup').hide();
  });
  
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
$('.remove-btn').click(function(){
  $('#rating_popup').hide(500);
  $('#submit_rate_button').hide();
});
  $('#submit_rate_button').on('click', function() {
    $('#submit_rate_button').prop('disabled', true).text('Please wait...');
    if($('#review').val() == '') {
      $('#submit_rate_button').prop('disabled', false).text('Submit');
      $("#review_error").show();
      return false;
    } else {
      $(".preloader").show();
      $.ajax({
        method: 'POST',
        url: '{{url('submit-rate')}}',
        data:{'date':$('#date').val(),'day_service_id':$('#day_service_id').val(),'booking_id':$('#booking_id').val(),
        'ratingValue':$('#ratingValue').val(),'review':$('#review').val(),_token:'{{csrf_token()}}'},
        success: function (result) {
            $(".preloader").hide();
            if(result.status == 'success'){
              $('.em-rating-set .em-next-btn').hide();
               $('#success-popup').show(500);
               if($('#ratingValue').val() == 5){
					window.setTimeout(function() {
						window.open("https://goo.gl/2cY7Vc","_self");
					}, 2000);
				}
               else{
                $('#rating_message').html('Thanks for your valuable feedback.');
               }
               $('#submit_rate_button').prop('disabled', true).text('Submitted');
            } else {
              $('#error-popup').show(500);   
              $('#submit_rate_button').prop('disabled', false).text('Submit');  
            }
        },
        error: function() {
          $('#error-popup').show(500);
          $('#submit_rate_button').prop('disabled', false).text('Submit');
        }
      });
    }
    });
  });

  
  $('#review').on('click', function() {
    $("#review_error").hide();
  });
  /* 2. Action to perform on click */
  $('#stars li').on('click', function() {
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }

    $('#ratingValue').val(ratingValue);
});


function responseMessage(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
</script>
@endpush
