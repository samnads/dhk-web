@extends('layouts.main', ['body_css_class' => 'home-page'])
@section('title', 'Home')
@section('content')
    <section class="banner-section">
        <!--<div class="container">-->
        <div class="row banner-wrapper m-0">
            <div id="banner" class="owl-carousel owl-theme p-0">
                @foreach ($api_data['banner_data'] as $key => $banner)
                    @if (@$banner['package_id'])
                        <div class="item position-relative" onclick="return false;" data-action="banner-select"
                            data-service_type_model_id="{{ @$banner['service_type_model_id'] }}"
                            data-service_type_model="{{ @$banner['service_type_model'] }}"
                            data-web_url_slug="{{ @$banner['web_url_slug'] }}" data-offer_id="{{ @$banner['offer_id'] }}"
                            data-coupon_code="{{ @$banner['coupon_code'] }}" data-package_id="{{ @$banner['package_id'] }}">
                            <a href="#">
                                <img src="{{ @$banner['banner_image_url'] ?: $banner['banner_image_url'] }}"
                                    class="object-fit_cover pc-view" alt="image" />
                                <img src="{{ @$banner['banner_image_url'] ?: $banner['banner_image_url'] }}"
                                    class="object-fit_cover mob-view" alt="image" />
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <!--</div>-->
    </section>
    @foreach ($api_data['service_categories'] as $category_key => $service_category)
        <section class="all-service-wrapper">
            <div class="container">
                <h3>{{ $service_category['service_type_category_name'] }}</h3>
                <div class="row m-0">
                    <div class="owl-carousel owl-theme p-0 service-categories">
                        @foreach ($service_category['sub_categories'] as $sub_category_key => $service)
                            <a href="#" onclick="return false;" data-action="service-select"
                                data-id="{{ $service['service_type_id'] }}"
                                data-service_type_model_id="{{ $service['service_type_model_id'] }}"
                                data-service_type_model="{{ $service['service_type_model'] }}"
                                data-web_url_slug="{{ $service['web_url_slug'] }}">
                                <div class="item position-relative">
                                    <div class="col-sm-12 service-image"><img
                                            src="{{ $service['service_type_thumbnail'] }}" class=""
                                            alt="{{ $service['service_type_name'] }}" /></div>
                                    <div class="col-sm-12 service-text">
                                        <h3>{{ $service['service_type_name'] }}</h3>
                                        {{-- @if (@$service['subscription_packages'])
                                            <button data-action="show-sub-packages"
                                                data-service_type_id="{{ $service['service_type_id'] }}"
                                                data-service_type_name="{{ $service['service_type_name'] }}"
                                                class="btn btn-sm btn-primary pull-right mb-3"
                                                style="background-color: #195880;">Packages <i class="fa fa-archive"
                                                    aria-hidden="true"></i></button>
                                        @endif --}}
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endforeach
    @if ($api_data['subscription_packages_and_special_offers'])
        <section class="all-service-wrapper">
            <div class="container">
                <h3>Subscription Packages</h3>
                <div class="row m-0">
                    <div class="owl-carousel owl-theme p-0 sub-packages">
                        @foreach ($api_data['subscription_packages_and_special_offers'] as $subscription_package)
                            @if (@$subscription_package['package_id'])
                                <a href="#" onclick="return false;" data-action="sub-package-select"
                                    data-id="{{ $subscription_package['package_id'] }}"
                                    data-web_url_slug="{{ $subscription_package['package_id'] }}">
                                    <div class="item position-relative">
                                        <div class="col-sm-12 service-image"><img
                                                src="{{ $subscription_package['banner_image_url'] }}" class=""
                                                alt="{{ $subscription_package['package_name'] }}" /></div>
                                        <div class="col-sm-12 service-text">
                                            <h3>{{ $subscription_package['package_name'] }}</h3>
                                        </div>
                                    </div>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    <section class="app-download-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 app-download-left v-center">
                    <div>
                        <h2>We are Available on<br />App Store & Google Play</h2>
                        <p>Download the app from Google Play or App Store and start enjoying the benefits right away.</p>
                        <a href="{{ $api_data['urls']['apps']['android'] }}"><img
                                src="{{ asset('images/android.jpg') }}?v=1.0" alt="" /></a>
                        <a href="{{ $api_data['urls']['apps']['ios'] }}"><img src="{{ asset('images/iOS.jpg') }}?v=1.0"
                                alt="" /></a>
                    </div>
                </div>
                <div class="col-sm-6 app-download-right"><img src="{{ asset('images/app.png') }}?v=1.0" alt="" />
                </div>
            </div>
        </div>
    </section>
    {{--@include('popups.sub-packages-popup')--}}
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript">
        var _subscription_packages = @json(@$api_data['subscription_packages'] ?: []);
    </script>
    <script type="text/javascript" src="{{ asset('js/home.js?v=') . Config::get('version.js') }}"></script>
@endpush
