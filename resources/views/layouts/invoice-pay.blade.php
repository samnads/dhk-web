 <!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">

 <head>
    @include('includes.google_tag')
     <title>Dubai Housekeeping - @yield('title', 'Default')</title>
     <!-- Required meta tags -->
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta name="theme-color" content="#195880">
     <meta name="csrf_token" content="{{ csrf_token() }}" />
     @include('includes.header_assets')
     @stack('styles')
 </head>

 <body class="{{@$body_css_class}}">
     @if (strpos($_SERVER['REQUEST_URI'], '-demo/') !== false || strpos($_SERVER['SERVER_NAME'], '127.0.0.1') !== false)
         <div class="demo-ribbon">
             <span>DEMO</span>
         </div>
     @endif
     <div class="wrapper-main">
         @include('includes.header')
         @yield('content')
         @include('includes.footer_assets')
         @include('includes.footer')
         @stack('scripts')
     </div>
 </body>

 </html>
