<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">

 <head>
    @include('includes.google_tag')
     <title>Dubai Housekeeping - @yield('title', 'Default')</title>
     <!-- Required meta tags -->
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta name="theme-color" content="#195880">
     <meta name="csrf_token" content="{{ csrf_token() }}" />
     @include('includes.header_assets')
     @stack('styles')
 </head>

 <body class="{{@$body_css_class}}">
     @if (strpos($_SERVER['REQUEST_URI'], 'demo/') !== false || strpos($_SERVER['SERVER_NAME'], '127.0.0.1') !== false)
         <div class="demo-ribbon">
             <span>DEMO {{date('h:i:s')}}</span>
         </div>
     @endif
     <div class="wrapper-main">

         @yield('content')
         @include('popups.new-address-popup')
         @include('popups.alert-popup')
        <script>
     var _base_url = "{{ url('') }}/";
    {{--var _api_url = "{{ Config::get('url.api_url') }}";--}}
    var _current_url = "{{ strtok(url()->full(), '?') }}";
</script>   
    <script type="text/javascript" src="{{ asset('js/jquery-3.7.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/js.cookie.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.query-object.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.jscroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fixie.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/24.3.6/js/intlTelInputWithUtils.min.js" integrity="sha512-byOJzE/JTCqPgJqqu/yVDaHSmpHlxGSm3PXirrI84E3gQqJXL62NTpMOz0CjtFGGi8HjINB4OzFlrVRvfReazA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="{{ asset('js/main.js?v=') . Config::get('version.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/login.js?v=') . Config::get('version.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0ztqRwv8zz7IMF7NTCcfF2MIw488DKR4&libraries=places&callback=google_maps_callback"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js" integrity="sha512-KGE6gRUEc5VBc9weo5zMSOAvKAuSAfXN0I/djLFKgomlIUjDCz3b7Q+QDGDUhicHVLaGPX/zwHfDaVXS9Dt4YA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    

         @stack('scripts')
     </div>
 </body>

 </html>
