@extends('layouts.main', [])
@section('title', 'Upcoming Bookings')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 top-steps-section">
                    <div class="d-flex page-title-section">
                        <div class="booking-page-title flex-grow-1">
                            <h3>Upcoming Bookings</h3>
                        </div>
                        <div class="step-back-icon"><a href="{{ url('profile') }}" class="back-arrow"
                                title="Click to Back">Back</a>
                        </div>
                        <div class="booking-steps pt-0">
                            <div class="booking-back ps-2"><a href="{{ url('bookings/past') }}" class="mt-0">Past
                                    Bookings</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 my-account-wrapper">
                    <div class="row m-0 pt-3">
                        @if (sizeof($booking_history['result']['completed_list']) == 0)
                            <div class="col-sm-8 m-auto p-0">
                                <div class="alert alert-info" role="alert">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    {{ $booking_history['result']['message'] }}
                                </div>
                            </div>
                        @else
                            <div class="col-sm-8 n-personal-details m-auto p-0">
                                @foreach ($booking_history['result']['completed_list'] as $key => $booking)
                                    <div
                                        class="co-sm-12 upcoming-booking-cont-main {{ strtolower(str_replace(' ', '-', $booking['status'])) }}">
                                        <input name="booking[]" value="{{ json_encode($booking) }}" type="hidden" />
                                        <div class="upcoming-booking-cont">
                                            <div class="row past-bookings-content m-0">
                                                <div class="col-sm-12 p-0">
                                                    <div class="row m-0">
                                                        <div class="col-md-8 past-bookings-content-left p-0">
                                                            <h4>{{ $booking['service'] }}</h4>
                                                            <p><i class="fa fa-calendar" aria-hidden="true"></i>
                                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d', $booking['date'])->format('d M, Y') }}

                                                                &nbsp; &nbsp; <i class="fa fa-clock-o"
                                                                    aria-hidden="true"></i>
                                                                {{ \Carbon\Carbon::createFromFormat('H:i', $booking['start_time'])->format('h:i A') }}
                                                            </p>
                                                            <p><i class="fa fa-repeat"
                                                                    aria-hidden="true"></i>&nbsp;{{ $booking['frequency'] }}
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4 past-bookings-content-right p-0">
                                                            <h4 class="text-right">{{ $booking['booking_reference'] }}</h4>
                                                            <p class="text-right mt-0">{{ $booking['status'] }}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 p-0">
                                                    <div class="row m-0">
                                                        <div class="col-md-8 past-bookings-content-left p-0">
                                                            <p class="bold"><i class="fa fa-money" aria-hidden="true"></i>
                                                                {{ $booking['payment_method'] }}
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4 past-bookings-content-right p-0">
                                                            <h4 class="text-right">
                                                                <b>{{ numberFormat($booking['_total_payable'] ?: $booking['total']) }}</b>
                                                                <span>AED</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 p-0 pt-2">
                                                    <div class="past-bookings-status-btn">
                                                        @if (@$booking['menu']['cancel_this'] === true)
                                                            <label class="cancelled" data-action="cancel-this-booking">
                                                                <a class="show-cancel-booking-popup">Cancel</a>
                                                            </label>
                                                        @endif
                                                        @if (@$booking['menu']['reschedule'] === true)
                                                            <label class="n-green-bg" data-action="reshedule">
                                                                <a class="">Reschedule</a>
                                                            </label>
                                                        @endif
                                                        @if (@$booking['menu']['change_payment_method'] === true)
                                                            <label class="n-blue-bg" data-action="change-pay-mode">
                                                                <a class="">Change Payment Mode</a>
                                                            </label>
                                                        @endif
                                                        @if (@$booking['menu']['retry_payment'] === true)
                                                            <label class="n-yellow-bg" data-action="retry-payment">
                                                                <a class="">Retry Payment</a>
                                                            </label>
                                                        @endif
                                                    </div>
                                                    <p class="upcoming-booked-on">Booked On: &nbsp; &nbsp; <i
                                                            class="fa fa-calendar" aria-hidden="true"></i>
                                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $booking['booked_date'])->format('d/m/Y') }}
                                                        &nbsp; &nbsp; <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        {{ \Carbon\Carbon::createFromFormat('H:i:s', $booking['booked_time'])->format('h:i A') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @include('popups.cancel-schedule-popup')
        @include('popups.reschedule-popup')
        @include('popups.change-pay-mode-popup')
        @include('popups.retry-pay-popup')
        @include('popups.booking-cancel-popup')
        @include('includes.ccavenue-hidden-form')
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>
    <script async src="https://pay.google.com/gp/p/js/pay.js" onload="google_pay_callback()"></script>
    <script type="text/javascript" src="{{ asset('js/bookings.js?v=') . Config::get('version.js') }}"></script>
@endpush
