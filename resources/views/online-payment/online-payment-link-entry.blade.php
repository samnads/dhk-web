@extends('layouts.invoice-pay', ['body_css_class' => 'booking-section-page'])
@section('title', 'Invoice Payment')
@section('content')
    <section>
        <div class="container">
            <form method="post" id="online_payment_link_form" name="online_payment_link_form" action="{{url('save-online-pay')}}" autocomplete="off" accept-charset="utf-8">
                {{ csrf_field() }}
                <input type="hidden" id="customerId" name="customerId" value="{{$customerId}}">
                <div class="row inner-wrapper enquiry-form-main v-center m-0">
                        <div class="col-lg-6 col-md-9 col-sm-12 enquiry-box m-auto">
                            <div class="col-sm-12 popup-head-text">
                                <h4>Make Payment</h4>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <p>Amount</p>
                                <input name="amount" class="text-field" type="number" step="any" placeholder="Enter amount in AED" value="{{@$amount}}" readonly required>
                            </div>
                            <div class="col-sm-12 text-field-main">
                                <p>Description</p>
                                <textarea name="description" cols="" rows="" class="text-field-big"
                                    placeholder="Enter description..." required>{{@$description}}</textarea>
                            </div>
                            <div class="col-sm-6 frequency-main pt-3">
                                <button class="text-field-btn" type="submit" id="payment_button">Make Payment</button>
                            </div>
                        </div>
                </div>
            </form>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/online-payment-link-entry.js?v=') . Config::get('version.js') }}"></script>
@endpush
