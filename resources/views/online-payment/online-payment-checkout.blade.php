@extends('layouts.invoice-pay', ['body_css_class' => 'booking-section-page'])
@section('title', 'Invoice Checkout')
@section('content')
    <section>
        <div class="container">
            <form id="online-pay-checkout-form" autocomplete="off" accept-charset="utf-8">
                {{ csrf_field() }}
                <input type="hidden" name="customer_id" value="{{ @$customerId }}">
                <input type="hidden" name="order_id" value="{{ @$save_invoice_pay['pay_details']['reference_id'] }}">
                <input type="hidden" name="amount" value="{{ @$save_invoice_pay['gross_amount'] }}">
                <div class="row inner-wrapper enquiry-form-main v-center m-0">
                    <div class="col-lg-6 col-md-9 col-sm-12 enquiry-box m-auto">
                        <div class="col-sm-12 popup-head-text">
                            <h4>Confirm Payment</h4>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <h3>Hi {{ @$save_invoice_pay['customer_name'] }},</h3>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <span>Please confirm your requested payment of <strong>AED
                                    {{ number_format(@$save_invoice_pay['amount'], 2, '.', '') }}</strong></span>
                        </div>
                        <div class="col-sm-12 border p-3">
                            <table class="payment_table">
                                <tbody>
                                    <tr class="borderless">
                                        <th colspan="2">Address</th>
                                    </tr>

                                    <tr>
                                        <td colspan="2">{{ @$save_invoice_pay['address'] }} ,
                                            {{ @$save_invoice_pay['area'] }}</td>
                                    </tr>

                                    <tr>
                                        <th>Service Amount</th>
                                        <td>AED {{ number_format(@$save_invoice_pay['amount'], 2, '.', '') }}</td>
                                    </tr>

                                    <tr>
                                        <th>Transaction Charge</th>
                                        <td>AED {{ number_format(@$save_invoice_pay['transaction_charge'], 2, '.', '') }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Payable Amount</th>
                                        <td>AED {{ number_format(@$save_invoice_pay['gross_amount'], 2, '.', '') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-3 mt-3 booking-form-list payment-method">
                            <ul id="payment-method-holder">
                                @foreach ($api_data['payment_types'] as $key => $payment_type)
                                    @if (!in_array($payment_type['id'], hidePaymentModes('invoice-payment')))
                                        @if ($payment_type['show_in_web'] == 1)
                                            <li class="pay-mode-li-{{ $payment_type['id'] }}">
                                                <input id="payment-method-{{ $payment_type['id'] }}"
                                                    value="{{ $payment_type['id'] }}" name="payment_method" class=""
                                                    type="radio">
                                                <label for="payment-method-{{ $payment_type['id'] }}">
                                                    <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                                    <img
                                                        src="{{ asset('images/payment-' . $payment_type['id'] . '.jpg?v=' . Config::get('version.img')) }}" />
                                                </label>
                                            </li>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn pull-right" id="submit-btn-pay-mode-0">
                            <button type="submit" class="text-field-btn">Continue</button>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn pull-right d-none" id="submit-btn-pay-mode-2">
                            <button type="submit" class="text-field-btn">Confirm</button>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn pull-right d-none" id="submit-btn-pay-mode-3">
                            <button type="submit" id="applePaybtn"></button>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn pull-right d-none" id="submit-btn-pay-mode-4">
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn d-none" id="submit-btn-pay-mode-5">
                            <button type="submit" class="text-field-btn" disabled>Tamara Go</button>
                        </div>
                        <!-- Telr -->
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn pull-right d-none" id="submit-btn-pay-mode-6">
                            <button type="submit" class="text-field-btn">Card Pay</button>
                        </div>
                        <!-- Telr -->
                        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn pull-right d-none" id="submit-btn-pay-mode-7">
                            <button type="submit" class="text-field-btn">Card + Apple Pay</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    @include('includes.ccavenue-hidden-form-dynamic', ['merchant_param1' => 'invoice-payment'])
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/online-payment-checkout.js?v=') . Config::get('version.js') }}">
    </script>
@endpush
