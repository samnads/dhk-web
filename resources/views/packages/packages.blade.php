@extends('layouts.main', ['body_css_class' => 'home-page'])
@section('title', 'Home')
@section('content')
    @if ($api_data['subscription_packages_and_special_offers'])
        <section class="all-service-wrapper">
            <div class="container">
                <h3>Subscription Packages</h3>
                <div class="row m-0">

                        @foreach ($api_data['subscription_packages_and_special_offers'] as $subscription_package)
                            @if (@$subscription_package['package_id'])
                                <div class="col-sm-4 pb-3">
                                <a href="#" onclick="return false;" data-action="sub-package-select"
                                    data-id="{{ $subscription_package['package_id'] }}"
                                    data-web_url_slug="{{ $subscription_package['package_id'] }}">

                                        <div class="col-sm-12 service-image"><img
                                                src="{{ $subscription_package['banner_image_url'] }}" class=""
                                                alt="{{ $subscription_package['package_name'] }}" /></div>
                                        <div class="col-sm-12 service-text">
                                            <h3>{{ $subscription_package['package_name'] }}</h3>
                                        </div>

                                </a>
                                </div>
                            @endif
                        @endforeach

                </div>
            </div>
        </section>
    @endif
@endsection
@push('styles')
<style>
.all-service-wrapper{padding: 90px 0px 20px 0px;}
</style>
@endpush
@push('scripts')
    <script type="text/javascript">
        var _subscription_packages = @json(@$api_data['subscription_packages'] ?: []);
    </script>
    <script type="text/javascript" src="{{ asset('js/home.js?v=') . Config::get('version.js') }}"></script>
@endpush
