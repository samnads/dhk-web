<div class="col-sm-12 top-steps-section step-2" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Checkout</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="2" class="back-arrow"
                title="Click to Back">Step
                2</a></div>
        <div class="booking-steps"> of 2</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12 payment-method-wrapper pb-2">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Payment method</h4>
        </div>
        <div class="col-sm-12 booking-form-list payment-method p-0">
            <ul id="payment-method-holder">
                @foreach ($api_data['payment_types'] as $key => $payment_type)
                    @if ($payment_type['show_in_web'] == 1)
                        @if (!in_array($payment_type['id'], hidePaymentModes('subs-package-model')))
                            <li class="pay-mode-li-{{ $payment_type['id'] }}">
                                <input id="payment-method-{{ $payment_type['id'] }}" value="{{ $payment_type['id'] }}"
                                    name="payment_method" class="" type="radio"
                                    {{ $payment_type['default'] == 1 ? 'checkedD' : '' }}>
                                <label for="payment-method-{{ $payment_type['id'] }}">
                                    <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                    <img
                                        src="{{ asset('images/payment-' . $payment_type['id'] . '.jpg?v=' . Config::get('version.img')) }}" />
                                </label>
                            </li>
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    @if ($api_service_type_data['data']['terms_and_conditions'])
        <div class="col-sm-12 p-0 pb-2 mt-3">
            <h4>Terms & Conditions</h4>
        </div>
        <div class="col-sm-12 mb-4">
            <div class="col-sm-12 m-4 mt-0">
                <ul>
                    @foreach ($api_service_type_data['data']['terms_and_conditions'] as $key => $point)
                        <li>{!! $point !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="col-sm-12">
        <div class="col-sm-12 p-0 pb-2">
            <p> <input type="checkbox" id="accept_terms" name="accept_terms" style="display: block"> I accept all the terms and conditions</p>
        </div>
    </div>
</div>
