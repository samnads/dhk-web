<link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/animation.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.toast.css?v=1.0') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css?v=') . Config::get('version.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=') . Config::get('version.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert2.min.css') }}">
<!--<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert2.material-ui.min.css') }}">-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/intlTelInput.css') }}">
