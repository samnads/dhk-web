<div class="col-lg-6 col-md-9 col-sm-12 enquiry-box m-auto">
    <div class="col-sm-12 popup-head-text">
        <h4>Enquiry Form</h4>
    </div>
    <div class="col-sm-12 enquiry-option">
        <ul>
            <li class="call-us-enquiry"><a href="tel:{{preg_replace('/[^A-Za-z0-9\-]/','',$api_data['mobile_numbers']['call'])}}" target="_blank"><i class="fa fa-phone"></i> Call
                    Us</a></li>
            <li class="whatsapp-enquiry"><a href="https://api.whatsapp.com/send?phone={{preg_replace('/[^A-Za-z0-9\-]/','',$api_data['mobile_numbers']['whatsapp'])}}" target="_blank"><i
                        class="fa fa-whatsapp"></i> Whatsapp</a></li>
            <li class="email-enquiry"><a href="mailto:info@example.com" target="_blank"><i class="fa fa-envelope-o"></i>
                    Email</a></li>
        </ul>
    </div>
    <!--<div class="col-sm-12 text-field-main">
        <p>Enquiry Note</p>
        <textarea name="note" cols="" rows="" class="text-field-big" placeholder="Enter your enquiry details here..."></textarea>
    </div>
    <div class="col-sm-6 frequency-main pt-3">
        <input value="Send Enquiry" class="text-field-btn" type="submit">
    </div>-->
</div>
