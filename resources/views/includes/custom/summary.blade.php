<div class="col-lg-4 col-md-5 booking-summary-section" id="booking-summary">
    <div class="col-sm-12 book-details-main mob-booking-title">
        <h3>Booking Summary</h3>
    </div>


    <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
        <div class="row m-0">
            <div class="col-sm-12 book-details-main mob-summary-title pb-2">
                <h3>Booking Summary</h3>
            </div>

            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p>{{ $api_service_type_data['data']['service_type_name'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Frequency</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="frequency">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Duration</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="duration">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Date & Time</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p class="date">-</p>
                        <p class="time"></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Number of Professionals</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p class="professionals_count">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Material</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="is_materials_included">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main instructions" style="display: none">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Instructions</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="instructions">-</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row book-details-main-set m-0 pt-5">
            <div class="col-sm-12 book-details-main pb-2">
                <h3>Payment Summary</h3>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="service_amount_before_discount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main service_discount" style="display: none;">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Discount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="service_discount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main service_amount" style="display: none;">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Net Service Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="service_amount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main supervisor_charge" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Supervisor Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="supervisor_charge">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main cleaning_materials_amount" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Material Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="cleaning_materials_amount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main tools_amount" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Tools Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="tools_amount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            {{--<div class="col-sm-12 book-details-main discount_total" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Discount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="discount_total">0.00</calc-amount></p>
                    </div>
                </div>
            </div>--}}
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Taxable Amount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="taxable_amount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>VAT</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="vat_amount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Total (Inc. VAT 5%)</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="taxed_amount">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main payment_type_charge" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Convenience Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><calc-amount class="payment_type_charge">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row total-price m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Total</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="total_payable">0.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 we-charge-info" style="display:none;">
                <div class="row m-0 pull-right">
                    <p class="text-info small"><i class="fa fa-info-circle" aria-hidden="true"></i> Charged first service only.</p>
                </div>
            </div>
        </div>
    </div>
</div>
