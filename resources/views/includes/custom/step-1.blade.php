<div class="col-sm-12 top-steps-section step-1">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Service Details</h3>
        </div>
        <div class="step-back-icon"><a href="{{ url('') }}" class="back-arrow" title="Click to Back">Step 1</a>
        </div>
        <div class="booking-steps"> of 4</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-1">
    <div class="col-sm-12 how-many-hours-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How many hours do you need your professional to stay?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="hours-count-holder">
                @for ($i = $api_service_type_data['data']['min_working_hours']; $i <= $api_service_type_data['data']['max_working_hours']; $i++)
                    <li><input id="hours-{{ $i }}" value="{{ $i }}" name="hours" class=""
                            type="radio">
                        <label for="hours-{{ $i }}">{{ $i }}</label>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
    <div class="col-sm-12 how-mainy-housekeepers-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How many professionals do you need?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="professionals-count-holder">
                @for ($i = $api_service_type_data['data']['min_no_of_professionals']; $i <= $api_service_type_data['data']['max_no_of_professionals']; $i++)
                    <li><input id="professionals-count-{{ $i }}" value="{{ $i }}"
                            name="professionals_count" class="" type="radio">
                        <label for="professionals-count-{{ $i }}">{{ $i }}</label>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper" id="supervisor-options"
        style="display: {{ $api_service_type_data['data']['settings']['supervisor_option_show'] == true ? 'block' : 'none' }}">
        >
        <div class="col-sm-12 p-0 pb-2">
            <h4>With Supervisor ?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="cleaning-materials-holder">
                <li><input id="supervisor-2" value="0" name="supervisor" class="" type="radio" checked>
                    <label for="supervisor-2">No</label>
                </li>
                <li><input id="supervisor-1" value="1" name="supervisor" class="" type="radio">
                    <label for="supervisor-1">Yes</label>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper"
        style="display: {{ $api_service_type_data['data']['settings']['cleaning_materials_option_show'] == true ? 'block' : 'none' }}">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Need cleaning materials?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="cleaning-materials-holder">
                <li><input id="cleaning-materials1" value="0" name="cleaning_materials" class=""
                        type="radio"
                        {{ $api_service_type_data['data']['settings']['cleaning_materials_opted'] == false ? 'checked' : '' }}>
                    <label for="cleaning-materials1">No, I have them</label>
                </li>
                <li><input id="cleaning-materials2" value="1" name="cleaning_materials" class=""
                        type="radio"
                        {{ $api_service_type_data['data']['settings']['cleaning_materials_opted'] == true ? 'checked' : '' }}>
                    <label for="cleaning-materials2">Yes, Please</label>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-12 mb-4" id="tools" style="display: none">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Select Tools</h4>
        </div>
        <div class="row booking-form-list p-0">
            @foreach ($api_service_type_data['data']['cleaning_materials']['no'] as $material)
                <div class="col-sm-2 col-6 checkbox_multi">
                    <input id="material-id-{{ $material['id'] }}" value="{{ $material['id'] }}" name="tool_ids"
                        data-type="{{ $material['type'] }}" class="" type="checkbox">
                    <label for="material-id-{{ $material['id'] }}">{{ $material['name'] }}</label>
                    @if ($material['option_description_html'])
                        <div id="option_description_{{ $material['id'] }}" class="text-muted">
                            <small class="fw-light">• {{ $material['option_description_html'] }}</small>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-12 mb-4" id="materials" style="display: none">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Cleaning Materials</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="materials-holder">
                @foreach ($api_service_type_data['data']['cleaning_materials']['yes'] as $material)
                    <li><input id="material-id-{{ $material['id'] }}" value="{{ $material['id'] }}"
                            name="material_ids" data-type="{{ $material['type'] }}" class="" type="radio"
                            {{ $api_service_type_data['data']['settings']['default_material_id'] == $material['id'] ? 'checked' : '' }}>
                        <label for="material-id-{{ $material['id'] }}">{{ $material['name'] }}</label>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row mb-4 {{ $api_service_type_data['data']['detail_questions'] ? '' : 'd-none' }}">
        @foreach ($api_service_type_data['data']['detail_questions'] as $question)
            <div class="col">
                <h4 class="pb-2">{{ $question['question'] }}</h4>
                <select id="question-id-{{ $question['id'] }}" data-question_id="{{ $question['id'] }}"
                    class="form-control text-field" name="answer_ids[]">
                    <option value="">-- Select --</option>
                    @foreach ($question['answers'] as $answer)
                        <option value="{{ $answer['id'] }}">{{ $answer['label'] }}</option>
                    @endforeach
                </select>
            </div>
        @endforeach
    </div>
    @if ($api_service_type_data['data']['what_to_expect'])
        <div class="col-sm-12 p-0 pb-2">
            <h4>What to expect from this service ?</h4>
        </div>
        <div class="col-sm-12 mb-4">
            <div class="col-sm-12 m-4 mt-0">
                <ul>
                    @foreach ($api_service_type_data['data']['what_to_expect'] as $key => $point)
                        <li>{!! $point['point_html'] !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
</div>
