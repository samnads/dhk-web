<div class="col-sm-12 top-steps-section step-3" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Date & Time</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="3" class="back-arrow" title="Click to Back">Step
                3</a></div>
        <div class="booking-steps"> of 4</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-3" style="display: none">
    <div class="col-sm-12 booking-alert-main">
        <div class="d-flex booking-alert">
            <div class="booking-alert-icon"><i class="fa fa-refresh"></i></div>
            <div class="booking-alert-cont flex-grow-1">
                <p><strong>Frequency</strong><br /><span class="frequency"></span></p>
            </div>
            <div class="booking-alert-btn" data-action="frequency-popup">Change</div>
        </div>
    </div>
    <div class="col-sm-12 calender-wrapper">
        <div class="col-sm-12 p-0">
            <h4>When would you like your service? </h4>
        </div>
        <div class="col-sm-12 calendar-main p-0">
            <div id="calendar" class="owl-carousel owl-theme p-0">
            </div>
        </div>
    </div>
    <div class="col-sm-12 what-time-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>What time would you like us to start?<!--<label class="what-time-btn">See All</label>--></h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="times-holder">
            </ul>
        </div>
    </div>
    <div class="col-sm-12" id="weekdays-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Select week days (optional)</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="weekdays-holder">
            </ul>
        </div>
    </div>
    <div class="col-sm-12 mt-3" id="recurring-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Recurring for how many months ?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="recurring-holder">
            </ul>
        </div>
    </div>
</div>