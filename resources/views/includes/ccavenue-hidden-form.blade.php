<form method="post" id="ccavenue-form" action="{{$api_data['ccavenue']['req_handler']}}" autocomplete="off" class="d-none" style="display: none;">
    {{csrf_field()}}
    <table width="40%" height="100" border='1' align="center">
        <caption>
            <font size="4" color="blue"><b>Integration Kit</b></font>
        </caption>
    </table>
    <table width="40%" height="100" border='1' align="center">
        <tr>
            <td>Parameter Name:</td>
            <td>Parameter Value:</td>
        </tr>
        <tr>
            <td colspan="2"> Compulsory information</td>
        </tr>
        <tr>
            <td>Merchant Id :</td>
            <td><input type="text" name="merchant_id" value="{{$api_data['ccavenue']['merchant_id']}}" readonly/></td>
        </tr>
        <tr>
            <td>Order Id :</td>
            <td><input type="text" name="order_id" readonly/></td>
        </tr>
        <tr>
            <td>Amount :</td>
            <td><input type="text" name="amount" readonly/></td>
        </tr>
        <tr>
            <td>Currency :</td>
            <td><input type="text" name="currency" value="AED" readonly/></td>
        </tr>
        <tr>
            <td>Redirect URL :</td>
            <td><input type="text" name="redirect_url" value="{{$api_data['ccavenue']['resp_handler']}}" readonly/></td>
        </tr>
        <tr>
            <td>Cancel URL :</td>
            <td><input type="text" name="cancel_url" value="{{$api_data['ccavenue']['resp_handler']}}" readonly/></td>
        </tr>
        <tr>
            <td>Language :</td>
            <td><input type="text" name="language" value="EN" readonly/></td>
        </tr>
        <tr>
            <td colspan="2">Merchant Params:</td>
        </tr>
        <tr>
            <td>merchant_param6 :</td>
            <td><input type="text" name="merchant_param1" value="web-booking"/></td>
        </tr>
        <tr>
            <td colspan="2">Billing information(optional):</td>
        </tr>
        <tr>
            <td>Billing Name :</td>
            <td><input type="text" name="billing_name" readonly/></td>
        </tr>
        <tr>
            <td>Billing Address :</td>
            <td><input type="text" name="billing_address" readonly/></td>
        </tr>
        <tr>
            <td>Billing City :</td>
            <td><input type="text" name="billing_city" readonly/></td>
        </tr>
        <tr>
            <td>Billing State :</td>
            <td><input type="text" name="billing_state" readonly/></td>
        </tr>
        <tr>
            <td>Billing Zip :</td>
            <td><input type="text" name="billing_zip" readonly/></td>
        </tr>
        <tr>
            <td>Billing Country :</td>
            <td><input type="text" name="billing_country" readonly/></td>
        </tr>
        <tr>
            <td>Billing Tel :</td>
            <td><input type="text" name="billing_tel" readonly/></td>
        </tr>
        <tr>
            <td>Billing Email :</td>
            <td><input type="text" name="billing_email" readonly/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="CheckOut"></td>
        </tr>
    </table>
</form>
