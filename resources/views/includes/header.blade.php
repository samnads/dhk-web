<header>
    <div class="container p-0">
        <div class="row m-0">
            <div class="col-lg-3 col-md-12 logo-main">
                <div class="logo"><a href="{{url('')}}" title="Click to Home"><img src="{{asset('images/logo.png')}}?v=1.0"
                            alt="" /></a></div>

                <div class="mobile-icon user-btn"><i class="fa fa-user"></i></div>

                <div class="mobile-dropdown">
                    <ul class="before-login" style="display: {{session('customer_id') ? 'none' : 'block'}}">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li><a href="javascript:void(0);" data-action="login-popup">Login</a></li>
                    </ul>
                    <ul class="after-login" style="display: {{session('customer_id') ? 'block' : 'none'}}">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li><a href="{{url('profile')}}">Account</a>
                            <ul>
                                <li><a href="#" onclick="return false;" data-action="logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>



                <div class="mobile-icon">
                    <a href="tel:{{$api_data['mobile_numbers']['call']}}"><i class="fa fa-phone"></i></a>
                </div>

                <div class="clear"></div>

            </div>
            <div class="col-lg-9 col-md-12 menu-section p-0">
                <nav id="primary_nav_wrap">
                    <ul class="before-login" style="display: {{session('customer_id') ? 'none' : 'block'}}">
                        <li><a href="tel:{{preg_replace('/[^A-Za-z0-9\-]/','',$api_data['mobile_numbers']['call'])}}"><i class="fa fa-phone"></i>&nbsp; {{str_replace(' ','',$api_data['mobile_numbers']['call'])}}</a></li>
                        <li><a href="{{url('')}}">Home</a></li>
                        <li><a href="javascript:void(0);" data-action="login-popup">Login</a></li>
                    </ul>
                    <ul class="after-login" style="display: {{session('customer_id') ? 'block' : 'none'}}">
                        <li><a href="tel:{{preg_replace('/[^A-Za-z0-9\-]/','',$api_data['mobile_numbers']['call'])}}"><i class="fa fa-phone"></i>&nbsp; {{str_replace(' ','',$api_data['mobile_numbers']['call'])}}</a></li>
                        <li><a href="{{url('')}}">Home</a></li>
                        <li><a href="{{url('profile')}}">Account</a>
                            <ul>
                                <li><a href="#" onclick="return false;" data-action="logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </nav>
            </div>
        </div>
    </div>
</header>