<div class="col-sm-12 top-steps-section step-1">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>{{ $service_type_name }}</h3>
        </div>
        <div class="step-back-icon"><a href="{{ url('') }}" class="back-arrow" title="Click to Back">Step 1</a>
        </div>
        <div class="booking-steps"> of 3</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-1">
    <div class="col-sm-12 booking-packages-nav-main">
        <div class="col-sm-12 booking-packages-nav sticker">
            <div class="booking-packages-categoty-scroll">
                <div id="package-category" class="owl-carousel owl-theme p-0">
                    @foreach ($api_package_data['packages'] as $group => $package)
                        <div class="item"><a href="#{{ strtolower($group) }}">{{ $group }}</a></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 booking-packages-cont-main pb-5">

        @foreach ($api_package_data['packages'] as $group => $packages)
            <div class="col-sm-12 booking-packages-category">
                <div class="col-sm-12 booking-packages-category-title pb-3">
                    <div id="{{ strtolower($group) }}" class="booking-category-nav-position">&nbsp;</div>
                    <h3>{{ $group }}</h3>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12 add-ons-banner"><img src="{{ $packages['banner_url'] }}" height=""
                            alt="" />
                    </div>
                    @foreach ($packages['data'] as $key => $package)
                        <div class="col-sm-12 booking-packages">
                            <input name="package[]" value="{{ json_encode($package) }}" type="hidden" />
                            <input id="package-{{ $package['package_id'] }}" value="{{ $package['package_id'] }}"
                                name="packages[]" class="" type="checkbox" data-service_time="{{ $package['service_time'] }}" data-building_type_id="{{ $package['building_type_id'] }}" data-no_of_maids="{{ $package['no_of_maids'] }}">
                            <label for="package-{{ $package['package_id'] }}" class="w-100">
                                <div class="col-sm-12 booking-packages-content-main d-flex">
                                    <div class="booking-packages-photo"><img src="{{ $package['thumbnail_url'] }}"
                                            alt="" />
                                    </div>
                                    <div class="booking-packages-cont v-center flex-grow-1">
                                        <h4>{{ $package['package_name'] }}</h4>
                                        <p>{{ $package['package_description'] }} <a href="javascript:void(0);" class="show-pak-det-popup" title="Click to more details">Read More</a></p>
                                    </div>
                                    <div class="booking-packages-price v-center">
                                        <label>AED <span>{{ $package['actual_amount'] }}</span>
                                            {{ $package['amount'] }}</label>
                                    </div>
                                    <div class="booking-packages-btn v-center">
                                        <div class="addon-btn-main">
                                            <a class="sp-btn">Add</a>
                                        </div>

                                        <div class="addon-btn-count">
                                            <input data-package_id="{{ $package['package_id'] }}"
                                                data-action="package-minus" value="" class="addon-btn-minus"
                                                type="button">
                                            <input data-package_id="{{ $package['package_id'] }}"
                                                id="package_quanity_{{ $package['package_id'] }}" min="0"
                                                max="{{ $package['cart_limit'] }}" name="package_quanity[]"
                                                value="0" class="addon-text-field no-arrow" type="number"
                                                readonly>
                                            <input data-package_id="{{ $package['package_id'] }}"
                                                data-action="package-plus" value="" class="addon-btn-plus"
                                                type="button">
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach

    </div>
    @if ($api_service_type_data['data']['what_to_expect'])
        <div class="col-sm-12 p-0 pb-2">
            <h4>What to expect from this service ?</h4>
        </div>
        <div class="col-sm-12 mb-4">
            <div class="col-sm-12 m-4 mt-0">
                <ul>
                    @foreach ($api_service_type_data['data']['what_to_expect'] as $key => $point)
                        <li>{!! $point['point_html'] !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
</div>
