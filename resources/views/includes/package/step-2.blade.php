<div class="col-sm-12 top-steps-section step-2" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Date & Time</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="2" class="back-arrow" title="Click to Back">Step
                2</a></div>
        <div class="booking-steps"> of 3</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12 calender-wrapper">
        <div class="col-sm-12 p-0">
            <h4>When would you like your service? </h4>
        </div>
        <div class="col-sm-12 calendar-main p-0">
            <div id="calendar" class="owl-carousel owl-theme p-0">
            </div>
        </div>
    </div>
    <div class="col-sm-12 what-time-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>What time would you like us to start?<!--<label class="what-time-btn">See All</label>--></h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="times-holder">
            </ul>
        </div>
    </div>
    <div class="row mb-4 {{$api_service_type_data['data']['detail_questions'] ? '' : 'd-none'}}">
        @foreach ($api_service_type_data['data']['detail_questions'] as $question)
            <div class="col">
                <h4 class="pb-2">{{ $question['question'] }}</h4>
                <select id="question-id-{{ $question['id'] }}" data-question_id="{{ $question['id'] }}"
                    class="form-control text-field" name="answer_ids[]">
                    <option value="">-- Select --</option>
                    @foreach ($question['answers'] as $answer)
                        <option value="{{ $answer['id'] }}">{{ $answer['label'] }}</option>
                    @endforeach
                </select>
            </div>
        @endforeach
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How to let the crew in?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <select class="form-control text-field" name="crew_in">
                <option value="">-- Select --</option>
                @foreach ($api_service_type_data['data']['crew_in_options'] as $option)
                    <option value="{{ $option['text'] }}">{{ $option['text'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>{{$api_service_type_data['data']['settings']['instructions_field_label']}}</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" rows="" class="text-field-big" spellcheck="false"
                placeholder="{{$api_service_type_data['data']['settings']['instructions_field_placeholder']}}" maxlength="300"></textarea>
                <p class="text-muted" id="ins-char-left">300 characters left</p>
        </div>
    </div>
</div>