<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
</head>

<body>
<div class="wrapper-main">
	<?php require_once('include/header.php') ?>
	<section>
		<div class="container">
			<div class="row inner-wrapper m-0">
				<div class="col-sm-12 top-steps-section">
					<div class="d-flex page-title-section">
						<div class="booking-page-title flex-grow-1">
							<h3>Furniture Cleaning</h3>
						</div>
						<div class="step-back-icon"><a href="index.php" class="back-arrow" title="Click to Back">Step 2</a></div>
						<div class="booking-steps"> of 4</div>
					</div>
				</div>
				
				
				<div class="col-lg-8 col-md-12 booking-form-left">
				
				
					<div class="col-sm-12 booking-packages-nav-main">
						<div class="col-sm-12 booking-packages-nav sticker">
						     
							<div class="booking-packages-categoty-scroll">

							<div id="package-category" class="owl-carousel owl-theme p-0">
							     <div class="item"><a href="#apartment">Apartment </a></div>
								 <div class="item"><a href="#villa">Villa</a></div>
								 <div class="item"><a href="#offers">Offers</a></div>
								 
								 <div class="item"><a href="#apartment">Apartment</a></div>
								 <div class="item"><a href="#villa">Villa</a></div>
								 <div class="item"><a href="#offers">Offers</a></div>
								 
								 <div class="item"><a href="#apartment">Apartment</a></div>
								 <!--<div class="item"><a href="#villa">Villa</a></div>
								 <div class="item"><a href="#offers">Offers</a></div>
								 
								 <div class="item"><a href="#apartment">Apartment</a></div>
								 <div class="item"><a href="#villa">Villa</a></div>
								 <div class="item"><a href="#offers">Offers</a></div>
								 -->
								 
								 
							</div>
							
							</div>
							
						</div>
					</div>
					
					
					<div class="col-sm-12 booking-packages-cont-main pb-5">
					
					
				   <div class="col-sm-12 booking-packages-category">
						<div class="col-sm-12 booking-packages-category-title pb-3">
						    <div id="apartment" class="booking-category-nav-position">&nbsp;</div>
							<h3>Apartment</h3>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-12 add-ons-banner"><img src="images/add-ons-banner.jpg" alt="" /></div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-apartment1" value="cash" name="packages-apartment" checked="checked" class="" type="checkbox">
								<label for="packages-apartment1">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-apartment2" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-apartment2">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes. Have your studio looking brand new deep and thorough cleaning in 180 minutes. Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-apartment3" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-apartment3">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
						</div>
				   </div>	 
					 
					 
				   <div class="col-sm-12 booking-packages-category">	 
						<div class="col-sm-12 booking-packages-category-title pb-3">
						    <div id="villa" class="booking-category-nav-position">&nbsp;</div>
							<h3>Villa</h3>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-12 add-ons-banner"><img src="images/add-ons-banner.jpg" alt="" /></div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-villa1" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-villa1">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-villa2" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-villa2">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-villa3" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-villa3">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
						</div>
				   </div>	 
					 
					 
					<div class="col-sm-12 booking-packages-category">	 
						<div class="col-sm-12 booking-packages-category-title pb-3">
							<div id="offers" class="booking-category-nav-position">&nbsp;</div>
							<h3>Offers</h3>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-12 add-ons-banner"><img src="images/add-ons-banner.jpg" alt="" /></div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-offers1" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-offers1">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-offers2" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-offers2">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
							<div class="col-sm-12 booking-packages">
								<input id="packages-offers3" value="cash" name="packages-apartment" class="" type="checkbox">
								<label for="packages-offers3">
								<div class="col-sm-12 booking-packages-content-main d-flex">
									<div class="booking-packages-photo"><img src="images/add-ons1.jpg" alt="" /></div>
									<div class="booking-packages-cont v-center flex-grow-1">
										<h4>Studio</h4>
										<p>Have your studio looking brand new deep and thorough cleaning in 180 minutes.<span class="show-pak-det-popup" title="Click to more details">Read More</span></p>
									</div>
									<div class="booking-packages-price v-center">
										<label>AED <span>456</span> 356</label>
									</div>
									<div class="booking-packages-btn v-center">
										<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
								</label>
							</div>
						</div>
					</div>
					
						
					</div>
					
				</div>
				
				
				<div class="col-lg-4 col-md-12 booking-summary-section">
					<div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
						<div class="row m-0">
							<div class="col-sm-12 book-details-main pb-3">
								<h3>Booking Summary</h3>
							</div>
							<div class="col-sm-12 book-details-main">
								<div class="row m-0">
									<div class="col-6 book-det-left ps-0 pe-0">
										<p>Studio</p>
									</div>
									<div class="col-6 book-det-right ps-0 pe-0">
										<div class="addon-btn-count">
											<input value="" class="addon-btn-minus" type="submit">
											<input name="" value="1" class="addon-text-field" type="text">
											<input value="" class="addon-btn-plus" type="submit">
										</div>
									</div>
								</div>
							</div>
							
							<!--
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service Details</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>3x Cupboard Cleaning</p></div>
								</div>
						   </div> 
                                      
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Frequency</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>One Time Service</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Duration</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>2 hours</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Date & Time</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>27 Sept 2023,<br />05:00 pm  to  07:00 pm</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-7 book-det-left ps-0 pe-0"><p>Number of Professionals</p></div>
									 <div class="col-5 book-det-right ps-0 pe-0"><p>5</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Material</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>No</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Crew</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>Elizabeth Delya</p></div>
								</div>
						   </div>  
						   
						   --> 
							
						</div>
						<div class="row book-details-main-set m-0 pt-5">
							<div class="col-sm-12 book-details-main pb-3">
								<h3>Payment Summary</h3>
							</div>
							
							<!--<div class="col-sm-12 promo-code-section">			
									  <div class="col-sm-12 promocode-main">
										   <div class="d-flex promocode-set show-promocode-popup">
											  <div class="promocode-icon"><img src="images/promo-code.png" alt="" /></div>
											  <div class="promocode-cont flex-grow-1"><p><strong>Promo Codes</strong><br /><span>No promocode selected.</span></p></div>
										   </div>
									  </div>
								</div>-->
							
							<div class="col-sm-12 book-details-main">
								<div class="row booking-amount m-0">
									<div class="col-6 book-det-left ps-0 pe-0">
										<p>Service Fee</p>
									</div>
									<div class="col-6 book-det-right ps-0 pe-0">
										<p><span>AED</span> 93.33</p>
									</div>
								</div>
							</div>
							<div class="col-sm-12 book-details-main">
								<div class="row booking-amount m-0">
									<div class="col-6 book-det-left ps-0 pe-0">
										<p>Total (inc VAT 5%)</p>
									</div>
									<div class="col-6 book-det-right ps-0 pe-0">
										<p><span>AED</span> 4.67</p>
									</div>
								</div>
							</div>
							<div class="col-sm-12 book-details-main">
								<div class="row total-price m-0">
									<div class="col-5 book-det-left ps-0 pe-0">
										<p>Total</p>
									</div>
									<div class="col-7 book-det-right ps-0 pe-0">
										<p><span>AED</span> 98.00</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 booking-main-btn-section">
					<div class="row m-0">
						<div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3"> <a href="date-and-time.php">
							<input value="Next" class="text-field-btn" type="submit">
							</a> </div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php require_once('include/footer.php') ?>






</body>
</html>
