	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

<link rel="stylesheet" type="text/css" href="css/datepicker.css"/>

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
			
				<div class="col-sm-12 top-steps-section">
					  <div class="d-flex page-title-section">
						  <div class="booking-page-title flex-grow-1"><h3>Checkout</h3></div>
						  <div class="step-back-icon"><a href="date-and-time.php" class="back-arrow" title="Click to Back">Step 4</a></div>
						  <div class="booking-steps"> of 4</div>
					  </div>
				 </div>


				 
				 <div class="col-lg-8 col-md-7 booking-form-left">
					  
					  
					  <div class="col-sm-12 payment-method-wrapper pb-2">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>Payment method</h4>
					       </div>
					       <div class="col-sm-12 booking-form-list payment-method p-0">
					            <ul>
								    <li class="cash-opt">
									    <input id="payment-method1" value="cash" name="payment-method" class="" type="radio">
                                        <label for="payment-method1"><p>Payment by</p>CASH</label>
									</li>
									
									<li class="card-opt">
									    <input id="payment-method2" value="card" name="payment-method" class="" type="radio">
                                        <label for="payment-method2"><p>Payment via</p>CARD</label>
									</li>
								</ul>
					       </div>
					  </div>
					  
					  
					  
					  <div class="col-sm-12 card-method-main mb-5">
					       <div class="row card-main-box m-0">
						        <div class="col-sm-12 card-method-field">
								     <p>Card Number</p>
								     <input name="" class="text-field" type="number">
								</div>
								
								<div class="col-sm-8 card-method-field">
								     <p>Exp. Date (MM/YY)</p>
								     <!--<input name="" class="text-field" type="number">-->
									 <input id="datepicker" class="text-field w90 calendar" name="datepicker" type="text" required="required" autocomplete="off" data-provide="datepicker">
								</div>
								
								<div class="col-sm-4 card-method-field">
								     <p>CVV Number</p>
								     <input name="" class="text-field" type="number" style="background: #FFF !important;">
								</div>
						   </div>
					  </div>
					  
					  
					  
					  
					  
					  <div class="col-sm-12 booking-alert-main booking-verification-alert">
						   <div class="d-flex booking-alert">
							  <div class="booking-alert-icon"><i class="fa fa-refresh"></i></div>
							  <div class="booking-alert-cont flex-grow-1"><p><strong>Card Verification Charge</strong><br /><span>AED 3.67 will be charged to verify your card. The amount will be refunded immediately.</span></p></div>
							  <div class="booking-alert-btn">Learn More</div>
							</div>
					  </div>
					  
					  
					  <div class="col-sm-12 booking-alert-main booking-guarantee-alert">
						   <div class="d-flex booking-alert">
							  <div class="booking-alert-icon"><i class="fa fa-thumbs-o-up"></i></div>
							  <div class="booking-alert-cont flex-grow-1"><p><strong>Our Guarantee</strong><br /><span>The session amount will be reserved on your card. You will only be charged once the session is completed.</span></p></div>
							  <div class="booking-alert-btn">Learn More</div>
							</div>
					  </div>
					  
					  
					  
					  <div class="col-sm-12">
					       <h4>Add a voucher code or Emaid credit</h4>
						   
						   <div class="row">
						        <div class="col-sm-4 voucher-code-credit position-relative">
								     <div class="voucher-close"><img src="images/el-close-white.png" alt=""></div>
								     <div class="col-sm-12 voucher-code-tmb position-relative">
									      <p class="pt-0">Voucher Code</p>
										  <p><strong>SUMMER - 50%</strong> <a href="#" class="pull-right">Details</a></p>
									 </div>
								</div>
								
								<div class="col-sm-4">
								     <div class="col-sm-12 voucher-code-tmb">
									      <p class="pt-0">Voucher Code</p>
										  <div class="addon-btn-main">
											  <a class="sp-btn show-promocode-popup">Add</a>
										  </div>
									 </div>
								</div>
						   </div>
						   
					  </div>
					  
					  
					  
					  
					  <div class="col-sm-12">
					       <div class="row book-details-main-set m-0 pt-5">
						   
							    <div class="col-sm-12 p-0 pb-3">
                                     <h3>Payment Summary</h3>
						        </div>
								
								
								<div class="col-sm-12 p-0 pb-2">
								  <div class="row booking-amount m-0">
									  <div class="col-7 book-det-left ps-0 pe-0"><p>Service Fee</p></div>
									  <div class="col-5 book-det-right ps-0 pe-0"><p><span>AED</span> 93.33</p></div>
								  </div>
								</div>
							  
							  
								<div class="col-sm-12 p-0 pb-2">
								  <div class="row booking-amount m-0">
									  <div class="col-7 book-det-left ps-0 pe-0"><p>Total (inc VAT 5%)</p></div>
									  <div class="col-5 book-det-right ps-0 pe-0"><p><span>AED</span> 4.67</p></div>
								  </div>
								</div>
							  
							  
							  
								<div class="col-sm-12 p-0 pb-2">
								  <div class="row total-price m-0">
									  <div class="col-7 book-det-left ps-0 pe-0"><p>Total</p></div>
									  <div class="col-5 book-det-right ps-0 pe-0"><p><span>AED</span> 98.00</p></div>
								  </div>
								</div>
								
						  </div>
					  </div>
				 
				 </div>
				 
				 
				 <div class="col-lg-4 col-md-5 booking-summary-section">
				      <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
					  <div class="row m-0">
					       <div class="col-sm-12 book-details-main pb-2">
                                <h3>Booking Summary</h3>
						   </div>
                                      
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>Home Cleaning</p></div>
								</div>
						   </div> 
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service Details</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>3x Cupboard Cleaning</p></div>
								</div>
						   </div> 
                                      
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Frequency</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>One Time Service</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Duration</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>2 hours</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Date & Time</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>27 Sept 2023,<br />05:00 pm  to  07:00 pm</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-7 book-det-left ps-0 pe-0"><p>Number of Professionals</p></div>
									 <div class="col-5 book-det-right ps-0 pe-0"><p>5</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Material</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>No</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Crew</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>Elizabeth Delya</p></div>
								</div>
						   </div>  
						   
						   </div>
						   
						   
						        
                      </div>
				 
				 </div>
				 
				 
				 <div class="col-sm-12 booking-main-btn-section">
				 <div class="row m-0">
						   <div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3">
						        <a href="booking-success.php">
							    <input value="Complete" class="text-field-btn" type="submit">
								</a>
						   </div>
					  </div>
				</div>
				 
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>

<script>
var dp=$("#datepicker").datepicker( {
    format: "mm-yyyy",
    startView: "months", 
    minViewMode: "months"
});

dp.on('changeMonth', function (e) {    
   //do something here
   //alert("Month changed");
});
	
</script>
          
</body>
</html>
