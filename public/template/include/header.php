<header>
    	<div class="container p-0">
              <div class="row m-0">
                  <div class="col-lg-3 col-md-12 logo-main">
                  		<div class="logo"><a href="index.php" title="Click to Home"><img src="images/logo.png" alt="" /></a></div>

                        <div class="mobile-icon user-btn"><i class="fa fa-user"></i></div>
						
						<div class="mobile-dropdown">
						     <ul>
								<li><a href="javascript:void(0);" class="show-login-popup">Login</a></li>
								<li><a href="my-account.php">My Account</a></li>
								<li><a href="index.php">Logout</a></li>
							 </ul>
						</div>
						
						
                        
                        <div class="mobile-icon">
                             <a href="tel:+97123456789"><i class="fa fa-phone"></i></a>
                        </div>
						
						<div class="clear"></div>
                        
                  </div>
                  <div class="col-lg-9 col-md-12 menu-section p-0">
                       
						 <nav id="primary_nav_wrap">
							<ul>
							
								<li><a href="tel:97123456789"><i class="fa fa-phone"></i>&nbsp; +971 2345 6789</a></li>
								
								
								
								<li><a href="index.php">Home</a></li>
								<li><a href="javascript:void(0);">Login</a>
									<ul>
										<li><a href="javascript:void(0);" class="show-login-popup">Login</a></li>
										<li><a href="my-account.php">My Account</a></li>
								        <li><a href="index.php">Logout</a></li>
									</ul>
								</li>            
							</ul>
							<div class="clear"></div>
						</nav>
                      
                  </div>
              </div>
        </div>
    </header>