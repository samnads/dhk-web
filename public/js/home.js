var owl = $(".service-categories");
owl.owlCarousel({
    nav: false,
    loop: false,
    dots: true,
    margin: 15,
    autoplay: true,
    autoplayTimeout: 4000,
    smartSpeed: 800,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 2
        },
        1100: {
            items: 2
        },
        1200: {
            items: 5,
            margin: 30
        }
    }
});
var owl = $("#banner");
owl.owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true
});
$('#sub-packages-popup [data-action="close"]').click(function () {
    hideSubPackagesPopup();
});
function hideSubPackagesPopup() {
    $('#sub-packages-popup').hide(500);
}
$('[data-action="show-sub-packages"]').click(function (e) {
    e.preventDefault();
    let service_type_id = $(this).attr('data-service_type_id');
    $('#sub-packages-popup .service-name').html($(this).attr('data-service_type_name'));
    $('#sub-packages-popup #sub-packages-holder .item').hide(); // hide all packages
    $('#sub-packages-popup #sub-packages-holder .item[data-service_type_id="'+service_type_id+'"]').show(); // show service type sub packages
    $('#sub-packages-popup').show(500);
    return false;
});
$('[data-action="book-sub-package"]').click(function () {
    let package_id = this.getAttribute('data-id');
    window.open(_base_url + 'package/' + package_id, "_self");
});
/**************************************************************** */
let owl_sub_packages = $(".sub-packages");
owl_sub_packages.owlCarousel({
    nav: false,
    loop: false,
    dots: true,
    margin: 15,
    autoplay: true,
    autoplayTimeout: 4000,
    smartSpeed: 800,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1,
            rows: 2
        },
        600: {
            items: 2,
            rows: 2
        },
        1000: {
            items: 2,
            rows: 2
        },
        1100: {
            items: 2,
            rows: 2
        },
        1200: {
            items: 5,
            margin: 30,
            rows: 2
        }
    }
});
$('[data-action="sub-package-select"]').click(function () {
    let package_id = this.getAttribute('data-id');
    window.open(_base_url + 'package/' + package_id, "_self");
});