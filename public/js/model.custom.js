// step button actions
//$('.step-1').hide();
//$('.step-3').show();
$('[data-action="next-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    _last_step = false;
    if (isStepValid(current_step) === false) {
        // DONT'T GO TO NEXT STEP
        return;
    }
    let next_step = current_step + 1;
    //
    if (next_step == 2) {
        refreshExtraServices();
    }
    if (next_step == 3) {
        //available_timeRender();
    }
    else if (next_step == 4) {
        if ($('input[name="id"]', booking_form).val() == "") {
            // Case 1 : not logged in
            showLogin();
            return false;
        }
        else if ($('input[name="address_id"]', booking_form).val() == "") {
            // Case 2 : no default address id found (may be because of no addrees in db)
            showNewAddress();
            toast('Add Address', 'Please add address to continue.', 'info');
            return false;
        }
    }
    //
    $('.step-' + current_step).hide();
    $('.step-' + next_step).show();
    // do after going to step
    /************************************* */
    // SEO
    let _meta_title_new = _meta_title + ' ' + next_step + '/4';
    $(document).prop('title', _meta_title_new);
    /************************************* */
    if (next_step == 4) {
        $('input[name="payment_method"]:checked', booking_form).trigger('change');
        _last_step = true;
    }
    //
    if (next_step == 3) {
        $('#frequency-popup').show(500);
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', next_step)));
    window.scrollTo(0, 0);
});
$('[data-action="prev-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    let prev_step = current_step - 1;
    //
    $('.step-' + current_step).hide();
    $('.step-' + prev_step).show();
    _last_step = false;
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + prev_step + '/4');
    /************************************* */
    if (prev_step == 3) {
        //$('#frequency-popup').show(500);
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', prev_step)));
    window.scrollTo(0, 0);
});
/************************************************************************************************* */
$(booking_form).submit(function () {
    // just to show last step error toasts
    isStepValid(4);
});
/************************************************************************************************* */
// steps validator
function isStepValid(step) {
    var required = [];
    if (step == 1) {
        required = [
            {
                name: 'hours',
                status: $('input[name="hours"]', booking_form).valid()
            },
            {
                name: 'professionals_count',
                status: $('input[name="professionals_count"]', booking_form).valid()
            },
            {
                name: 'cleaning_materials',
                status: $('input[name="cleaning_materials"]', booking_form).valid()
            },
            {
                name: 'material_ids',
                status: $('input[name="cleaning_materials"]:checked').val() == 1 ? $('input[name="material_ids"]', booking_form).valid() : true,
            },
            {
                name: 'answer_ids[]',
                status: $('select[name="answer_ids[]"]').length > 0 ? $('select[name="answer_ids[]"]', booking_form).valid() : true
            },
        ];
    }
    else if (step == 2) {
        required = [
            {
                name: 'crew_in',
                status: $('select[name="crew_in"]', booking_form).valid()
            },
            {
                name: 'instructions',
                status: $('textarea[name="instructions"]', booking_form).valid()
            },
        ];
    }
    else if (step == 3) {
        required = [
            {
                name: 'date',
                status: $('input[name="date"]', booking_form).valid()
            },
            {
                name: 'time',
                status: $('input[name="time"]', booking_form).valid()
            },
        ];
    }
    else if (step == 4) {
        required = [
            {
                name: 'payment_method',
                status: $('input[name="payment_method"]', booking_form).valid()
            },
            {
                name: 'accept_terms',
                status: $('input[name="accept_terms"]', booking_form).valid()
            },
        ];
    }
    /************************************************ */
    // check for errors and show as toast
    var errors = required.filter(field => {
        return field.status === false
    });
    if (errors.length > 0) {
        var first_error_field = errors[0].name;
        var error_message = booking_form_validator.submitted[first_error_field];
        toast(null, error_message, 'info');
        scrollToElement($('[name="' + first_error_field + '"]', booking_form));
        return false;
    }
    /************************************************ */
    // step valid
    return true;
}
/************************************************************************************************* */
function renderTimes(available_times) {
    var times_html = ``;
    // Check if available_times is empty
    if (available_times.length === 0) {
        times_html = `<div class="alert alert-info" role="alert">
                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No time slots available for the selected date!
                    </div>`;
    } else {
        // Generate HTML for available times
        $.each(available_times, function (index, time) {
            times_html += `<li>
                        <input id="time-`+ index + `" value="` + time + `" name="time" class="" type="radio">
                        <label for="time-`+ index + `">
                            <!--<p>AED 5 Extra</p>-->`+ moment(time, "HH:mmm").format("hh:mm A") + `
                        </label>
                    </li>`;
        });
    }
    $('#times-holder').html(times_html);
    $('#booking-form input[name="time"]').change(function () {
        var hours = $('#booking-form input[name="hours"]:checked').val();
        var summary_time_from = moment(this.value, 'H:mm').format('hh:mm A');
        var summary_time_to = moment(this.value, 'H:mm').add(hours, 'hours').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from + ` to ` + summary_time_to);
        calculate();
    });
    calculate();
    //$('#booking-form input[name="time"]:first').trigger('click');
}
available_time_req = null;
function available_timeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        data: {
            date: $('input[name="date"]:checked', booking_form).val(),
            hours: $('input[name="hours"]:checked', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
available_datetime_req = null;
function available_datetimeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_datetime_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_datetime",
        data: {
            service_type_id: $('input[name="service_type_id"]', booking_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_datetime_req != null) {
                available_datetime_req.abort();
            }
        },
        success: function (response) {
            var dates_html = ``;
            $.each(response.result.available_dates, function (index, date) {
                let disabled = response.result.disabled_dates.find(disabled_date => {
                    return disabled_date == date
                });
                disabled = disabled ? 'disabled' : '';
                title = disabled ? 'Holiday' : '';
                dates_html += `<div class="item">
                    <input data-weekday="${moment(date, 'YYYY-MM-DD').day()}" id="date-`+ index + `" value="` + moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') + `" name="date" class="" type="radio" ` + disabled + `>
                    <label for="date-`+ index + `" title="` + title + `">
                        <div class="calendar-tmb-main">
                            <div class="calendar-tmb-day">`+ moment(date, 'YYYY-MM-DD').format('ddd') + `</div>
                            <div class="calendar-tmb-date">`+ moment(date, 'YYYY-MM-DD').format('DD') + `</div>
                            <div class="calendar-tmb-month">`+ moment(date, 'YYYY-MM-DD').format('MMM') + `</div>
                        </div>
                    </label>
                </div>`;
            });
            $('#calendar').html(dates_html);
            $('#booking-form input[name="date"]').change(function () {
                let summary_date_obj = moment(this.value, 'DD/MM/YYYY');
                let summary_date = summary_date_obj.format('DD MMM YYYY');
                $('#booking-summary .date').html(summary_date);
                available_timeRender();
                weekdaysOption();
            });
            $('#calendar.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
            $('#calendar.owl-carousel').find('.owl-stage-outer').children().unwrap();
            $('#calendar.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
            $("#calendar.owl-carousel").owlCarousel({
                nav: true,
                loop: false,
                dots: false,
                margin: 15,
                autoplay: false,
                autoplayTimeout: 2000,
                smartSpeed: 800,
                autoplayHoverPause: false,
                responsive: {
                    0: {
                        items: 5
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 10
                    },
                    1100: {
                        items: 10
                    },
                    1200: {
                        items: 10,
                        //margin: 50
                    }
                }
            }); //re-initialise the owl
            $('input[name="date"]:not([disabled]):first', booking_form).trigger('click');
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
// frequency popup
$('#frequency-popup .popup-close').click(function () {
    $('#frequency-popup').hide(500);
});
$('input[name="frequency"]').change(function () {
    // frequency selected
    let frequency = $('input[name="frequency"]:checked').val();
    frequency_sel = _service_type_data.frequency_list.find(freq => {
        return freq.code == frequency
    });
    $('.frequency').html(frequency_sel.name);
    if (frequency_sel.coupon_code == null) {
        // frequency have no coupon code
        if (Cookies.get('coupon_code')) {
            // take from cookie if found
            _cart.coupon_code = Cookies.get('coupon_code');
        }
    }
    else {
        // frequency copon found
        Cookies.remove('coupon_code'); // remove from cookie
        _cart.coupon_code = frequency_sel.coupon_code;
    }
    if(frequency_sel.code == 'WE'){
        $('.we-charge-info').show();
    }
    else{
        $('.we-charge-info').hide();
    }
    weekdaysOption();
    recurringMonthOption();
    calculate();
});
function weekdaysOption() {
    let frequency = $('input[name="frequency"]:checked').val();
    let week_day = $('input[name="date"]:checked').attr('data-weekday');
    $('input[name="weekdays"]').prop('disabled', false);
    $('input[name="weekdays"]').prop('checked', false);
    if (frequency == 'WE') {
        $('input[name="weekdays"][value="' + week_day + '"]').prop('checked', true).trigger('change');
        $('input[name="weekdays"][value="' + week_day + '"]').prop('disabled', true);
        $('#weekdays-wrapper').show();
    }
    else {
        $('input[name="weekdays"]').prop('disabled', true);
        $('#weekdays-wrapper').hide();
    }
}
function recurringMonthOption() {
    let frequency = $('input[name="frequency"]:checked').val();
    $('input[name="recurring_months"]').prop('disabled', false);
    if (frequency == 'WE') {
        $('input[name="recurring_months"][data-default="1"]').prop('checked', true);
        $('#recurring-wrapper').show();
    }
    else {
        $('input[name="recurring_months"]').prop('checked', false);
        $('input[name="recurring_months"]').prop('disabled', true);
        $('#recurring-wrapper').hide();
    }
}
$('[data-action="frequency-select"]').click(function () {
    // frequency selected
    let frequency = $('input[name="frequency"]:checked').val();
    frequency_sel = _service_type_data.frequency_list.find(freq => {
        return freq.code == frequency
    });
    $('.frequency').html(frequency_sel.name);
    if (frequency_sel.coupon_code == null) {
        // frequency have no coupon code
        if (Cookies.get('coupon_code')) {
            // take from cookie if found
            _cart.coupon_code = Cookies.get('coupon_code');
        }
    }
    else {
        // frequency copon found
        Cookies.remove('coupon_code'); // remove from cookie
        _cart.coupon_code = frequency_sel.coupon_code;
    }
    $('#frequency-popup').hide(500);
    calculate();
});
$('[data-action="frequency-popup"]').click(function () {
    $('#frequency-popup').show(500);
});
/************************************************************************************************* */
function superVisorOptions() {
    let hours = $('input[name="hours"]:checked', booking_form).val();
    let maids = $('input[name="professionals_count"]:checked', booking_form).val();
    if (hours in _service_type_data.supervisor.hours_match && maids in _service_type_data.supervisor.maids_match) {
        // Enable and Show
        $('input[name="supervisor"]').prop('disabled', false);
        $('#supervisor-options').show(500);
    }
    else {
        // Check NO and Disable
        $('input[name="supervisor"][value="0"]').prop('checked', true);
        $('input[name="supervisor"]').prop('disabled', true);
        $('#supervisor-options').hide(500);
    }
}
/************************************************************************************************* */
/**
 * Detail question answers
 */
$('select[name="answer_ids[]"]').change(function () {
    _cart.details = [];
    $('select[name="answer_ids[]"]', booking_form).each(function () {
        if ($(this).val()) {
            var detail = {
                question_id: $(this).attr('data-question_id'),
                answer_id: $(this).val(),
            }
            _cart.details.push(detail);
        }
    });
    calculate();
});
/************************************************************************************************* */
$('input[name="supervisor"]').change(function () {
    calculate();
});
/************************************************************************************************* */
$('input[name="hours"]').click(function () {
    $('#booking-summary .duration').html(this.value + ' Hours');
    superVisorOptions();
    refreshExtraServices();
    available_timeRender();

});
$('input[name="professionals_count"]').change(function () {
    $('#booking-summary .professionals_count').html(this.value);
    superVisorOptions();
    calculate();
});
function showTools() {
    if (_service_type_data.cleaning_materials.no.length) {
        $('#tools').show(500);
    }
}
$('input[name="cleaning_materials"]').change(function () {
    $('#booking-summary .is_materials_included').html(this.value == 1 ? 'Yes' : 'No');
    _cart.tool_ids = [];
    if (this.value == 1) {
        // Tools - Disable | Uncheck
        $('input[name="tool_ids"][data-type="Custom"]').prop('checked', false).prop('disabled', true);
        // Materials - Enable
        $('input[name="material_ids"][data-type="PlanBased"]').prop('checked', false).prop('disabled', false);
        $('#tools').hide(500);
        $('#materials').show(500);
    }
    else {
        // Materials - Disable | Uncheck
        $('input[name="material_ids"][data-type="PlanBased"]').prop('checked', false).prop('disabled', true);
        // Tools - Enable | Show
        $('input[name="tool_ids"][data-type="Custom"]').prop('checked', false).prop('disabled', false);
        $('#materials').hide(500);
        showTools();
    }
    calculate();
});
$('input[name="tool_ids"]').change(function () {
    _cart.tool_ids = [];
    $('input[name="tool_ids"]:checked', booking_form).each(function (index, tool) {
        _cart.tool_ids.push($(this).val());
    });
    calculate();
});
$('input[name="material_ids"]').change(function () {
    calculate();
});
function refreshExtraServices() {
    let hours = $('input[name="hours"]:checked', booking_form).val();
    if (_service_type_data.extra_services.length > 0) {
        $.each(_service_type_data.extra_services, function (index, service) {
            if (hours >= service.show_on_min_hour && hours <= service.show_on_max_hour) {
                // Show
                $('input[name="extra_services[]"][value="' + service.id + '"]').closest('.owl-item').show();
            }
            else {
                // Uncheck | Hide
                $('input[name="extra_services[]"][value="' + service.id + '"]').prop('checked', false);
                $('input[name="extra_services[]"][value="' + service.id + '"]').closest('.owl-item').hide();
            }
        });
    }
}
function renderExtraServices() {
    var extra_services_html = ``;
    if (_service_type_data.extra_services.length == 0) {
        $('.extra-services-wrapper').hide();
    }
    else {
        $.each(_service_type_data.extra_services, function (index, service) {
            extra_services_html += `<div class="item">
                    <input id="extra-service-`+ service.id + `" value="` + service.id + `" name="extra_services[]" class=""
                        type="checkbox">
                    <label for="extra-service-`+ service.id + `">
                        <div class="add-ons-scroll-thumb">
                            <div class="add-ons-scroll-image"><img src="`+ service.image_url + `"
                                    alt="" /></div>
                            <div class="add-ons-scroll-cont-main">
                                <div class="add-ons-scroll-cont">
                                    <h4>`+ service.service + `</h4>
                                    <p><strong><!--Messy cupboards?--></strong>
                                        `+ service.duration + ` Minutes<!--<a href="#">Learn
                                            More</a>--></p>
                                </div>
                                <div class="add-ons-scroll-price">AED ` + service.cost + `</div>
                            </div>
                        </div>
                    </label>
                </div>`;
        });
        $('#extra-services-holder').html(extra_services_html);
        $('#extra-services-holder.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
        $('#extra-services-holder.owl-carousel').find('.owl-stage-outer').children().unwrap();
        $('#extra-services-holder.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
        $("#extra-services-holder.owl-carousel").owlCarousel({
            nav: false,
            loop: false,
            dots: true,
            margin: 15,
            autoplay: false,
            autoplayTimeout: 2000,
            smartSpeed: 800,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1100: {
                    items: 3
                },
                1200: {
                    items: 3,
                    //margin: 50
                }
            }
        }); //re-initialise the owl
    }
    $('input[name="extra_services[]"]', booking_form).change(function () {
        _cart.extra_services = [];
        $('input[name="extra_services[]"]:checked', booking_form).each(function () {
            var item = {
                id: $(this).val()
            }
            _cart.extra_services.push(item);
        });
        calculate();
    });
}
$().ready(function () {
    if (_coupon_code != null) {
        _cart.coupon_code = _coupon_code;
    }
    if (Cookies.get('coupon_code')) {
        _cart.coupon_code = Cookies.get('coupon_code');
    }
    $(".scroll").jScroll();
    /************************************************************************************ */
    $('#booking-form input[name="hours"][value="' + _service_type_data.default_no_of_hours + '"]').trigger('click');
    $('#booking-form input[name="professionals_count"]:first').trigger('click');
    $('#booking-form input[name="cleaning_materials"]:checked').trigger('change');
    if(_service_type_data.settings.default_material_id != null){
        // check default option
        $('input[name="material_ids"][value="'+_service_type_data.settings.default_material_id+'"]',booking_form).prop('checked', true);
    }
    /************************************************************************************ */
    available_datetimeRender();
    let frequency = $('input[name="frequency"]:checked').val();
    frequency_sel = _service_type_data.frequency_list.find(freq => {
        return freq.code == frequency
    });
    $('.frequency').html(frequency_sel.name);
    /************************** extra services render  */
    renderExtraServices();
    /************************** addons render  */
    /*var addons_html = ``;
    if (_service_type_data.addons.length == 0) {
        $('.add-ons-wrapper').hide();
        $('#title-step-2').html('Instructions');
    }
    else {
        $.each(_service_type_data.addons, function (index, addon) {
            addons_html += `<div class="item">
                    <input id="addon_`+ addon.service_addons_id + `" value="` + addon.service_addons_id + `" name="addons[]" class=""
                        type="checkbox">
                    <label for="addon_`+ addon.service_addons_id + `">
                        <div class="add-ons-scroll-thumb">
                            <div class="add-ons-scroll-image"><img src="`+ addon.image_url + `"
                                    alt="" /></div>
                            <div class="add-ons-scroll-cont-main">
                                <div class="add-ons-scroll-cont">
                                    <h4>`+ addon.service_addon_name + `</h4>
                                    <p><strong><!--Messy cupboards?--></strong>
                                        `+ addon.service_addon_description + `<!--<a href="#">Learn
                                            More</a>--></p>
                                </div>
                                <div class="add-ons-scroll-price">AED <span> `+ addon.strike_amount + ` </span> ` + addon.amount + `</div>
                                <div class="add-ons-scroll-btn">
                                    <div class="addon-btn-main">
                                        <a class="sp-btn">Add</a>
                                    </div>
                                    <div class="addon-btn-count">
                                        <input data-addons_id="` + addon.service_addons_id + `" data-action="addon-minus" class="addon-btn-minus" type="button">
                                        <input
                                        id="addons_quanity_` + addon.service_addons_id + `"
                                        data-addons_id="` + addon.service_addons_id + `"
                                        min="0" max="` + addon.cart_limit + `"
                                        name="addons_quanity[]"
                                        value="0"
                                        class="addon-text-field no-arrow"
                                        type="number" readonly>
                                        <input data-addons_id="` + addon.service_addons_id + `" data-action="addon-plus" class="addon-btn-plus" type="button">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>`;
        });
        $('#service-addons-holder').html(addons_html);
        $('#service-addons-holder.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
        $('#service-addons-holder.owl-carousel').find('.owl-stage-outer').children().unwrap();
        $('#service-addons-holder.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
        $("#service-addons-holder.owl-carousel").owlCarousel({
            nav: false,
            loop: false,
            dots: true,
            margin: 15,
            autoplay: false,
            autoplayTimeout: 2000,
            smartSpeed: 800,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1100: {
                    items: 3
                },
                1200: {
                    items: 3,
                    //margin: 50
                }
            }
        }); //re-initialise the owl
    }*/
    /************************************************************************************************* */
    /*$('#booking-form input[name="addons[]"]').change(function () {
        // direct ui click change handler
        if (this.checked) {
            $('input[type="number"][data-addons_id="' + this.value + '"]').val(1);
        }
        else {
            $('input[type="number"][data-addons_id="' + this.value + '"]').val(0);
        }
        addonsToggled();
    });*/
    /************************************************************************************************* */
    /*function addonsToggled() {
        _cart.addons = [];
        $('#booking-form input[name="addons[]"]:checked').each(function (index, obj) {
            var addon = {
                service_addons_id: $(this).val(),
                quantity: $('#addons_quanity_' + $(this).val()).val(),
            }
            _cart.addons.push(addon);
        });
        calculate();
    }*/
    /************************************************************************************************* */
    /*$('[data-action="addon-plus"]').click(function () {
        current_quantity = Number($('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val());
        max_quantity = $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').attr("max");
        if (current_quantity >= max_quantity) {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(max_quantity)
        }
        else {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(current_quantity + 1)
        }
        addonsToggled();
    });*/
    /************************************************************************************************* */
    /*$('[data-action="addon-minus"]').click(function () {
        current_quantity = Number($('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val());
        min_quantity = $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').attr("min");
        updated_quantity = current_quantity - 1;
        if (updated_quantity == 0) {
            $('#addon_' + $(this).attr("data-addons_id")).prop('checked', false).trigger('change');
        }
        else if (updated_quantity > min_quantity) {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(updated_quantity);
        }
        else {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(min_quantity);
        }
        addonsToggled();
    });*/
    /************************************************************************************************* */
    /**
     * Render week days
     */
    let week_days_html = '';
    $.each(_service_type_data.week_days, function (index, week_day) {
        week_days_html += `<li>
        <input id="weekday-${week_day.week_day_id}" value="${week_day.week_day_id}" name="weekdays" class="" type="checkbox"/>
        <label for="weekday-${week_day.week_day_id}">${week_day.week_name}</label>
        </li>`
    });
    $('#weekdays-holder').html(week_days_html);
    $('input[name="weekdays"]', booking_form).change(function () {
        _cart.weekdays = [];
        $('input[name="weekdays"]', booking_form).each(function () {
            if ($(this).is(':checked:not(:disabled)')) {
                _cart.weekdays.push($(this).val());
            }
        });
        calculate();
    });
    weekdaysOption();
    /**
     * Recurring months
     */
    let recurring_months_html = '';
    $.each(_service_type_data.recurring_periods, function (index, recurring_month) {
        recurring_months_html += `<li>
        <input data-default="${recurring_month.default || ''}" id="recurring-month-${recurring_month.id}" value="${recurring_month.no_of_months}" name="recurring_months" class="" type="radio"/>
        <label for="recurring-month-${recurring_month.id}" title="${recurring_month.no_of_weeks_label}">${recurring_month.no_of_months_label}</label>
        </li>`
    });
    $('#recurring-holder').html(recurring_months_html);
    $('input[name="recurring_months"]', booking_form).change(function () {
        calculate();
    });
    recurringMonthOption();
    /************************************************************************************************* */
    create_booking_req = null;
    booking_form_validator = $('#booking-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "date": {
                required: true,
            },
            "time": {
                required: true,
            },
            "hours": {
                required: true,
            },
            "professionals_count": {
                required: true,
            },
            "cleaning_materials": {
                required: true,
            },
            "material_ids": {
                required: function (element) {
                    return $('input[name="cleaning_materials"]:checked').val() == 1;
                }
            },
            "answer_ids[]": {
                required: true,
            },
            "crew_in": {
                required: true,
            },
            "instructions": {
                required: _service_type_data.settings.instructions_field_required,
            },
            "payment_method": {
                required: true,
            },
            "accept_terms": {
                required: true,
            }
        },
        messages: {
            "date": {
                required: "Select service date",
            },
            "time": {
                required: "Select service time",
            },
            "hours": {
                required: "Select service hours",
            },
            "professionals_count": {
                required: "Select no. of professionals",
            },
            "cleaning_materials": {
                required: "Select materials option",
            },
            "material_ids": {
                required: "Select material",
            },
            "answer_ids[]": {
                required: "Select answer",
            },
            "crew_in": {
                required: "Select crew in option",
            },
            "instructions": {
                required: "Type any instructions",
            },
            "payment_method": {
                required: "Select payment method",
            },
            "accept_terms": {
                required: "Accept Terms & Conditions",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "date") {
                error.insertAfter($('#calendar').parent());
            }
            else if (element.attr("name") == "time") {
                error.insertAfter($('#times-holder').parent());
            }
            else if (element.attr("name") == "hours") {
                error.insertAfter($('#hours-count-holder').parent());
            }
            else if (element.attr("name") == "professionals_count") {
                error.insertAfter($('#professionals-count-holder').parent());
            }
            else if (element.attr("name") == "cleaning_materials") {
                error.insertAfter($('#cleaning-materials-holder').parent());
            }
            else if (element.attr("name") == "material_ids") {
                error.insertAfter($('#materials-holder').parent());
            }
            else if (element.attr("name") == "instructions") {
                error.insertAfter($('#booking-form textarea[name="instructions"]').parent());
            }
            else if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else if (element.attr("name") == "accept_terms") {
                error.insertAfter($('#booking-form input[name="accept_terms"]').parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            booking_form = form;
            createBooking();
            return false;
        }
    });
});
/*********************************************************************************************/
/*debug = true;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};*/
/*var merchantIdentifier = "merchant.com.emaid.elitmaidsUser.livennew";
if (window.ApplePaySession) {
    var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
    promise.then(function (canMakePayments) {
        if (canMakePayments) {
            // Display Apple Pay button here.
            logit('hi, I can do ApplePay');
        }
    });
} else {
    logit('ApplePay is not available on this browser');
}
/*********************************************************************************************/
var session = null; // apple pay session store
function createBooking() {
    booking_form = $('#booking-form');
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    if (payment_method == 1) {
        // continue
    }
    else if (payment_method == 2) {
        // continue
    }
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html);
    booking_btn.attr('disabled', true);
    create_booking_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/create_booking",
        dataType: 'json',
        data: {
            address_id: $('input[name="address_id"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            hours: $('input[name="hours"]:checked', booking_form).val(),
            professionals_count: $('input[name="professionals_count"]:checked', booking_form).val(),
            cleaning_materials: $('input[name="cleaning_materials"]:checked', booking_form).val(),
            frequency: $('input[name="frequency"]:checked', booking_form).val(),
            coupon_code: _cart.coupon_code,
            date: $('input[name="date"]:checked', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            payment_method: $('input[name="payment_method"]:checked', booking_form).val(),
            instructions: $('textarea[name="instructions"]', booking_form).val(),
            supervisor: $('input[name="supervisor"]:checked', booking_form).val(),
            cleaning_material_id: $('input[name="material_ids"]:checked', booking_form).val(),
            tool_ids: _cart.tool_ids,
            details: _cart.details,
            extra_services: _cart.extra_services,
            crew_in: $('select[name="crew_in"]', booking_form).val(),
            weekdays: _cart.weekdays,
            recurring_months: $('input[name="recurring_months"]:checked', booking_form).val()
        },
        beforeSend: function () {
            if (create_booking_req != null) {
                create_booking_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (_calculation_data.input.payment_method == 1) {
                    booking_btn.attr('disabled', false);
                    booking_btn.html('Completed');
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
                else if (_calculation_data.input.payment_method == 2) {
                    // ccavenue
                    booking_btn.html('Please wait...');
                    $('[name="order_id"]',ccavenue_form).val(response.result.billing.order_id);
                    $('[name="amount"]',ccavenue_form).val(response.result.billing.amount);
                    $('[name="billing_name"]',ccavenue_form).val(response.result.billing.name);
                    $('[name="billing_address"]',ccavenue_form).val(response.result.billing.address);
                    $('[name="billing_city"]',ccavenue_form).val(response.result.billing.city);
                    $('[name="billing_state"]',ccavenue_form).val(response.result.billing.state);
                    $('[name="billing_zip"]',ccavenue_form).val(response.result.billing.zip);
                    $('[name="billing_country"]',ccavenue_form).val(response.result.billing.country);
                    $('[name="billing_tel"]',ccavenue_form).val(response.result.billing.tel);
                    $('[name="billing_email"]',ccavenue_form).val(response.result.billing.email);
                    ccavenue_form.submit();
                }
            }
            else {
                booking_btn.attr('disabled', false);
                booking_btn.html('Complete');
                create_booking_error(response);
            }
        },
        error: function (response) {
            ajaxError(null, response);
            //booking_btn.attr('disabled', false);
        },
    });
}