$().ready(function () {
    /************************************************************************** */
    const baseRequest = {
        apiVersion: 2,
        apiVersionMinor: 0
    };
    const tokenizationSpecification = {
        type: 'PAYMENT_GATEWAY',
        parameters: {
            'gateway': 'checkoutltd',
            'gatewayMerchantId': _checkout_primary_key
        }
    };
    const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];
    const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];
    const baseCardPaymentMethod = {
        type: 'CARD',
        parameters: {
            allowedAuthMethods: allowedCardAuthMethods,
            allowedCardNetworks: allowedCardNetworks
        }
    };
    const cardPaymentMethod = Object.assign(
        { tokenizationSpecification: tokenizationSpecification },
        baseCardPaymentMethod
    );
    const paymentsClient = new google.payments.api.PaymentsClient({ environment: _google_pay_environment });
    const isReadyToPayRequest = Object.assign({}, baseRequest);
    isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];
    paymentsClient.isReadyToPay(isReadyToPayRequest)
        .then(function (response) {
            if (response.result) {
                // add a Google Pay payment button
            }
        })
        .catch(function (err) {
            // show error in developer console for debugging
            console.error(err);
        });
    const button =
        paymentsClient.createButton({
            buttonColor: 'black',
            buttonType: 'plain',
            buttonSizeMode: 'fill',
            onClick: () => {
                openPopup();
            },
            allowedPaymentMethods: []
        });
    document.getElementById('google-pay-button').appendChild(button);
    /*********************************************************************************************/
    function openPopup(){
        const baseRequest = {
            apiVersion: 2,
            apiVersionMinor: 0
        };
        const tokenizationSpecification = {
            type: 'PAYMENT_GATEWAY',
            parameters: {
                'gateway': 'checkoutltd',
                'gatewayMerchantId': _checkout_primary_key
            }
        };
        const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];
        const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];
        const baseCardPaymentMethod = {
            type: 'CARD',
            parameters: {
                allowedAuthMethods: allowedCardAuthMethods,
                allowedCardNetworks: allowedCardNetworks
            }
        };
        const cardPaymentMethod = Object.assign(
            { tokenizationSpecification: tokenizationSpecification },
            baseCardPaymentMethod
        );
        const paymentsClient = new google.payments.api.PaymentsClient({ environment: _google_pay_environment });
        const isReadyToPayRequest = Object.assign({}, baseRequest);
        isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];
        paymentsClient.isReadyToPay(isReadyToPayRequest)
            .then(function (response) {
                if (response.result) {
                    // add a Google Pay payment button
                }
            })
            .catch(function (err) {
                // show error in developer console for debugging
                console.error(err);
            });
        const paymentDataRequest = Object.assign({}, baseRequest);
        paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
        paymentDataRequest.transactionInfo = {
            //totalPriceStatus: 'NOT_CURRENTLY_KNOWN',
            totalPriceStatus: 'FINAL',
            totalPrice: '1',
            currencyCode: 'AED',
            countryCode: 'AE'
        };
        paymentDataRequest.merchantInfo = {
            merchantName: 'Elite Advance Building Cleaning LLC',
            merchantId: 'BCR2DN4TXLQ3FHS6'
        };
        paymentsClient.loadPaymentData(paymentDataRequest).then(function (paymentData) {
            // if using gateway tokenization, pass this token without modification
            //paymentToken = paymentData.paymentMethodData.tokenizationData.token;
            //_checkout_token_data = paymentToken;
            //createBooking();
            // continue
        }).catch(function (err) {
            // show error in developer console for debugging
            console.error(err);
            //booking_btn.attr('disabled', false);
        });
    }
    openPopup();
});