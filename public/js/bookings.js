let reschedule_form = $('#reschedule-popup-form');
let change_pay_mode_form = $('#change-pay-mode-popup-form');
let retry_pay_form = $('#retry-pay-popup-form');
let cancel_schedule_form = $('#cancel-schedule-popup-form');
let booking_cancel_form = $('#booking-cancel-form');
let ccavenue_form = $('#ccavenue-form');
var _checkout_token_data = undefined;
let cancel_booking_div;
function showCancelPopup() {
    $('#booking-cancel-confirm').show(500);
}
function hideCancelPopup() {
    $('#booking-cancel-confirm').hide(500);
}
$().ready(function () {
    booking_cancel_form_validator = $('#booking-cancel-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "booking_id": {
                required: true,
            },
            "date": {
                required: true,
            },
            "cancel_reason": {
                required: true,
            }
        },
        messages: {
            "booking_id": {
                required: "Booking ID required",
            },
            "date": {
                required: "Date required",
            },
            "cancel_reason": {
                required: "Select cancel reason",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "cancel_reason") {
                error.insertAfter(element);
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/booking_cancel",
                dataType: 'json',
                data: booking_cancel_form.serialize(),
                success: function (response) {
                    submit_btn.html('Yes').prop("disabled", false);
                    if (response.result.status == "success") {
                        cancel_booking_div.remove();
                        hideCancelPopup();
                        toast('Cancelled', response.result.message, 'success');
                    } else {
                        toast('Failed', response.result.message, 'error');
                    }
                },
                error: function (response) {
                    submit_btn.html('Yes').prop("disabled", false);
                },
            });
        }
    });
    $('#booking-cancel-confirm [data-action="close"]').click(function () {
        hideCancelPopup();
    });
    $('.show-cancel-booking-popup').click(function () {
        booking_cancel_form = $('#booking-cancel-form');
        let booking = JSON.parse($(this).closest('.upcoming-booking-cont-main').find("input[name='booking[]']").val());
        booking_cancel_form_validator.resetForm(); // remove errors
        $('[name="booking_id"]', booking_cancel_form).val(booking.booking_id);
        $('[name="date"]', booking_cancel_form).val(moment(booking.date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#cancel-booking-ref').html(booking.booking_reference);
        $('#cancel-booking-date').html(moment(booking.date, 'YYYY-MM-DD').format('DD MMM, YYYY'));
        $('#cancel-booking-frequency').html(booking.frequency);
        $('[name="cancel_reason"]', booking_cancel_form).val('');
        cancel_booking_div = $(this).closest('.upcoming-booking-cont-main');
        showCancelPopup();
    });
    $('input[name="payment_method"]', change_pay_mode_form).change(function () {
        let payment_method = this.value; // current mode
        $("div[class*='payment-type-']", change_pay_mode_form).hide();
        $('.payment-type-' + payment_method, change_pay_mode_form).show();
        $('input[name="PaymentMethod"]', change_pay_mode_form).val(payment_method);
        _checkout_token_data = undefined;
        $('[id^=change-btn-pay-mode-]').hide();
        if (payment_method == 1) {
            // cash
            $('button[type="submit"]', change_pay_mode_form).html("Complete");
        }
        else if (payment_method == 2) {
            // card
            $('button[type="submit"]', change_pay_mode_form).html("Pay Now");
        }
        $('#change-btn-pay-mode-' + payment_method).show();
    });
    $('[data-action="cancel-this-bookin"]').click(function () {
        let booking = JSON.parse($(this).closest('.upcoming-booking-cont-main').find("input[name='booking[]']").val());
        //showCancelPopup(booking);
        let div = $(this).closest('.upcoming-booking-cont-main');
        Swal.fire({
            title: "Confirm cancel?",
            html: `<div class="d-flex booking-alert">
                    <div class="booking-alert-icon"><i class="fa fa-exclamation-triangle text-danger"></i></div>
                    <div class="booking-alert-cont flex-grow-1">
                        <p><strong>Cancellation Policy</strong></p>
                        <p><span>Free cancellation within 12 hours, 50% charge applies between 12 hours and the time of booking.</span></p>
                    </div>
                </div>`,
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "lightgreen",
            confirmButtonText: "Yes, cancel it!",
            cancelButtonText: "No",
            focusCancel: true
        }).then((result) => {
            if (result.isConfirmed) {
                let end_point = 'booking_cancel';
                $.ajax({
                    type: 'POST',
                    url: _base_url + "api/customer/" + end_point,
                    data: {
                        booking_id: booking.booking_id,
                        date: moment(booking.date, 'YYYY-MM-DD').format('DD/MM/YYYY'),
                    },
                    success: function (response) {
                        if (response.result.status == "success") {
                            toast('Cancelled', response.result.message, 'success');
                            div.remove();
                        }
                        else {
                            toast('Error', response.result.message, 'error');
                        }
                    },
                    error: function (response) {
                    },
                });
            }
        });
    });
    $('[data-action="reshedule"]').click(function () {
        let booking = JSON.parse($(this).closest('.upcoming-booking-cont-main').find("input[name='booking[]']").val());
        showReschedulePopup(booking);
    });
    $('[data-action="change-pay-mode"]').click(function () {
        let booking = JSON.parse($(this).closest('.upcoming-booking-cont-main').find("input[name='booking[]']").val());
        showPayModePopup(booking);
    });
    $('[data-action="retry-payment"]').click(function () {
        let booking = JSON.parse($(this).closest('.upcoming-booking-cont-main').find("input[name='booking[]']").val());
        showRetryPayPopup(booking);
    });
    $('.upcoming-booking-cont-main').click(function () {
        let booking = JSON.parse($(this).find("input[name='booking[]']").val());
    });
    $('#reschedule-popup [data-action="close"]').click(function () {
        location.reload(); // ui issue with owl corousel
        hideReschedulePopup();
    });
    $('#change-pay-mode-popup [data-action="close"]').click(function () {
        hidePayModePopup();
    });
    reschedule_form_validator = $('#reschedule-popup-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "date": {
                required: true,
            },
            "time_from": {
                required: true,
            }
        },
        messages: {
            "date": {
                required: "Select reschedule date",
            },
            "time_from": {
                required: "Select reschedule time",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "date") {
                error.insertAfter($('#calendar').append());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter($('#times-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', reschedule_form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/reschedule",
                dataType: 'json',
                data: reschedule_form.serialize(),
                success: function (response) {
                    submit_btn.html('Reschedule').prop("disabled", false);
                    if (response.result.status == "success") {
                        hideReschedulePopup();
                        Swal.fire({
                            title: "Rescheduled !",
                            html: response.result.message,
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
                    } else {
                        toast('Failed', response.result.message, 'error');
                    }
                },
                error: function (response) {
                    submit_btn.html('Reschedule').prop("disabled", false);
                },
            });
        }
    });
    change_pay_mode_validator = $('#change-pay-mode-popup-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "payment_method": {
                required: true,
            },
            "card_number": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "exp_date": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "cvv": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
        },
        messages: {
            "payment_method": {
                required: "Select payment mode",
            },
            "card_number": {
                required: "Enter card number",
            },
            "exp_date": {
                required: "Enter card expiry date",
            },
            "cvv": {
                required: "Enter CVV code",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let payment_method = $('input[name="payment_method"]:checked', change_pay_mode_form).val();
            change_pay_mode_form = $('#change-pay-mode-popup-form');
            let submit_btn = $('button[type="submit"]', change_pay_mode_form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/change_payment_mode",
                dataType: 'json',
                data: {
                    booking_id: $('input[name="booking_id"]', change_pay_mode_form).val(),
                    PaymentMethod: $('input[name="PaymentMethod"]', change_pay_mode_form).val(),
                    checkout_token_data: _checkout_token_data
                },
                success: function (response) {
                    if (response.result.status == "success") {
                        if (payment_method == 1) {
                            // cash
                            hidePayModePopup();
                            Swal.fire({
                                title: "Payment Mode Changed !",
                                html: response.result.message,
                                icon: "success",
                                allowOutsideClick: false,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            });
                        }
                        else if (payment_method == 2) {
                            // ccavenue
                            submit_btn.html('Please wait...');
                            $('[name="order_id"]', ccavenue_form).val(response.result.billing.order_id);
                            $('[name="amount"]', ccavenue_form).val(response.result.billing.amount);
                            $('[name="billing_name"]', ccavenue_form).val(response.result.billing.name);
                            $('[name="billing_address"]', ccavenue_form).val(response.result.billing.address);
                            $('[name="billing_city"]', ccavenue_form).val(response.result.billing.city);
                            $('[name="billing_state"]', ccavenue_form).val(response.result.billing.state);
                            $('[name="billing_zip"]', ccavenue_form).val(response.result.billing.zip);
                            $('[name="billing_country"]', ccavenue_form).val(response.result.billing.country);
                            $('[name="billing_tel"]', ccavenue_form).val(response.result.billing.tel);
                            $('[name="billing_email"]', ccavenue_form).val(response.result.billing.email);
                            ccavenue_form.submit();
                        }
                    } else {
                        submit_btn.html(resetPayBtnName(payment_method)).prop("disabled", false);
                        //toast('Failed', response.result.message, 'error');
                        hidePayModePopup();
                        Swal.fire({
                            title: "Failed !",
                            html: response.result.message,
                            icon: "error",
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                location.reload();
                            }
                        });
                    }
                },
                error: function (response) {
                    submit_btn.html('Complete').prop("disabled", false);
                },
            });
        }
    });
    retry_pay_form_validator = $('#retry-pay-popup-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "payment_method": {
                required: true,
            },
        },
        messages: {
            "payment_method": {
                required: "Select payment mode",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let payment_method = $('input[name="payment_method"]', retry_pay_form).val();
            retry_pay_form = $('#retry-pay-popup-form');
            let submit_btn = $('button[type="submit"]', retry_pay_form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/retry_payment",
                dataType: 'json',
                data: {
                    booking_id: $('input[name="booking_id"]', retry_pay_form).val(),
                    PaymentMethod: $('input[name="payment_method"]', retry_pay_form).val(),
                    checkout_token_data: _checkout_token_data
                },
                success: function (response) {
                    if (response.result.status == "success") {
                        if (payment_method == 1) {
                            // cash
                            // do nothing
                            return false;
                        }
                        else if (payment_method == 2) {
                            // card
                            // ccavenue
                            submit_btn.html('Please wait...');
                            $('[name="order_id"]', ccavenue_form).val(response.result.billing.order_id);
                            $('[name="amount"]', ccavenue_form).val(response.result.billing.amount);
                            $('[name="billing_name"]', ccavenue_form).val(response.result.billing.name);
                            $('[name="billing_address"]', ccavenue_form).val(response.result.billing.address);
                            $('[name="billing_city"]', ccavenue_form).val(response.result.billing.city);
                            $('[name="billing_state"]', ccavenue_form).val(response.result.billing.state);
                            $('[name="billing_zip"]', ccavenue_form).val(response.result.billing.zip);
                            $('[name="billing_country"]', ccavenue_form).val(response.result.billing.country);
                            $('[name="billing_tel"]', ccavenue_form).val(response.result.billing.tel);
                            $('[name="billing_email"]', ccavenue_form).val(response.result.billing.email);
                            ccavenue_form.submit();
                        }
                    } else {
                        submit_btn.html(resetPayBtnName(payment_method)).prop("disabled", false);
                        //toast('Failed', response.result.message, 'error');
                        //hidePayModePopup();
                        hideRetryPayPopup();
                        Swal.fire({
                            title: "Failed !",
                            html: response.result.message,
                            icon: "error",
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                location.reload();
                            }
                        });
                    }
                },
                error: function (response) {
                    submit_btn.html('Complete').prop("disabled", false);
                },
            });
        }
    });
    /************************************************************************** */
    const baseRequest = {
        apiVersion: 2,
        apiVersionMinor: 0
    };
    const tokenizationSpecification = {
        type: 'PAYMENT_GATEWAY',
        parameters: {
            'gateway': 'checkoutltd',
            'gatewayMerchantId': _checkout_primary_key
        }
    };
    const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];

    const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];

    const baseCardPaymentMethod = {
        type: 'CARD',
        parameters: {
            allowedAuthMethods: allowedCardAuthMethods,
            allowedCardNetworks: allowedCardNetworks
        }
    };
    const cardPaymentMethod = Object.assign(
        { tokenizationSpecification: tokenizationSpecification },
        baseCardPaymentMethod
    );

    const paymentsClient = new google.payments.api.PaymentsClient({ environment: _google_pay_environment });

    const isReadyToPayRequest = Object.assign({}, baseRequest);
    isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];

    paymentsClient.isReadyToPay(isReadyToPayRequest)
        .then(function (response) {
            if (response.result) {
                // add a Google Pay payment button
            }
        })
        .catch(function (err) {
            // show error in developer console for debugging
            console.error(err);
        });

    const retry_button =
        paymentsClient.createButton({
            buttonColor: 'black',
            buttonType: 'plain',
            buttonSizeMode: 'fill',
            onClick: () => {
                retry_pay_form.submit();
            },
            allowedPaymentMethods: []
        });
    const change_button =
        paymentsClient.createButton({
            buttonColor: 'black',
            buttonType: 'plain',
            buttonSizeMode: 'fill',
            onClick: () => {
                change_pay_mode_form.submit();
            },
            allowedPaymentMethods: []
        });
    document.getElementById('retry-btn-pay-mode-4').appendChild(retry_button);
    document.getElementById('change-btn-pay-mode-4').appendChild(change_button);
    /*********************************************************************************************/
    // apple pay avilability check
    var merchantIdentifier = "merchant.com.emaid.elitmaidsUser.livennew";
    if (window.ApplePaySession) {
        var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
        promise.then(function (canMakePayments) {
            if (canMakePayments) {
                // Display Apple Pay button here.
                logit('hi, I can do ApplePay');
            }
        });
        $('.pay-mode-li-6').hide(); // hide card (Telr)
    } else {
        logit('ApplePay is not available on this browser');
        $('.pay-mode-li-3').hide(); // hide apple pay
        $('.pay-mode-li-7').remove(); // hide apple pay (Telr)
    }
    /*********************************************************************************************/
});
debug = false;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};
available_datetime_req = null;
function available_datetimeRender() {
    reschedule_form = $('#reschedule-popup-form');
    $('#times-holder', reschedule_form).html(loading_html);
    available_datetime_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_datetime",
        data: {
            service_type_id: $('input[name="service_type_id"]', reschedule_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_datetime_req != null) {
                available_datetime_req.abort();
            }
        },
        success: function (response) {
            var dates_html = ``;
            $.each(response.result.available_dates, function (index, date) {
                let disabled = response.result.disabled_dates.find(disabled_date => {
                    return disabled_date == date
                });
                disabled = disabled ? 'disabled' : '';
                title = disabled ? 'Holiday' : '';
                dates_html += `<div class="item">
                    <input id="date-`+ index + `" value="` + moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') + `" name="date" class="" type="radio" ` + disabled + `>
                    <label for="date-`+ index + `" title="` + title + `">
                        <div class="calendar-tmb-main">
                            <div class="calendar-tmb-day">`+ moment(date, 'YYYY-MM-DD').format('ddd') + `</div>
                            <div class="calendar-tmb-date">`+ moment(date, 'YYYY-MM-DD').format('DD') + `</div>
                            <div class="calendar-tmb-month">`+ moment(date, 'YYYY-MM-DD').format('MMM') + `</div>
                        </div>
                    </label>
                </div>`;
            });
            $('#calendar', reschedule_form).html(dates_html);
            $('input[name="date"]', reschedule_form).change(function () {
                available_timeRender();
            });
            $('#calendar.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
            $('#calendar.owl-carousel').find('.owl-stage-outer').children().unwrap();
            $('#calendar.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
            $("#calendar.owl-carousel").owlCarousel({
                nav: true,
                loop: false,
                dots: false,
                margin: 15,
                autoplay: false,
                smartSpeed: 100,
                responsive: {
                    0: {
                        items: 5
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 10
                    },
                    1100: {
                        items: 10
                    },
                    1200: {
                        items: 10,
                        //margin: 50
                    }
                }
            }); //re-initialise the owl
            $('input[name="date"]:first', reschedule_form).trigger('click');
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
function renderTimes(available_times) {
    reschedule_form = $('#reschedule-popup-form');
    var times_html = ``;
    $.each(available_times, function (index, time) {
        times_html += `<li>
                    <input id="time-`+ index + `" value="` + time + `" name="time_from" class="" type="radio">
                    <label for="time-`+ index + `">
                        <!--<p>AED 5 Extra</p>-->`+ moment(time, "HH:mmm").format("hh:mm A") + `
                    </label>
                </li>`;
    });
    if (times_html == '') {
        times_html = `<div class="alert alert-info" role="alert">
                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No time slots available for the selected date!
                    </div>`;
    }
    $('#times-holder', reschedule_form).html(times_html);
    $('input[name="time"]', reschedule_form).change(function () {
    });
}
available_time_req = null;
function available_timeRender() {
    reschedule_form = $('#reschedule-popup-form');
    $('#times-holder', reschedule_form).html(loading_html);
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        data: {
            date: $('input[name="date"]:checked', reschedule_form).val(),
            hours: $('input[name="hours"]', reschedule_form).val(),
            service_type_id: $('input[name="service_type_id"]', reschedule_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
function showReschedulePopup(booking) {
    reschedule_form = $('#reschedule-popup-form');
    $('.booking_id', reschedule_form).html(booking.booking_reference);
    $('input[name="booking_id"]', reschedule_form).val(booking.booking_id);
    $('input[name="service_type_id"]', reschedule_form).val(booking.service_type_id);
    let hour_in_seconds = moment(booking.hours, 'HH:mm').diff(moment().startOf('day'), 'seconds');
    $('input[name="hours"]', reschedule_form).val(hour_in_seconds / 3600);
    available_datetimeRender();
    $('#reschedule-popup').show(500);
}
function hideReschedulePopup() {
    $('input[name="booking_id"]', reschedule_form).val(''); // reset
    $('#reschedule-popup').hide(500);
}
function showPayModePopup(booking) {
    change_pay_mode_form = $('#change-pay-mode-popup-form');
    let payment_type_id = booking.payment_type_id; // current mode
    $('input[name="booking_id"]', change_pay_mode_form).val(booking.booking_id);
    $("li[id*='payment-mode-']", change_pay_mode_form).show(); // show all
    $('input[name="payment_method"]', change_pay_mode_form).prop("checked", false).trigger('change'); // unselect
    $('#payment-mode-' + payment_type_id, change_pay_mode_form).hide(); // hide current mode
    change_pay_mode_validator.resetForm();
    $('#change-pay-mode-popup').show(500);
}
/******************************************************************************* */
$('#retry-pay-popup [data-action="close"]').click(function () {
    hideRetryPayPopup();
});
function showRetryPayPopup(booking) {
    retry_pay_form.trigger("reset");
    retry_pay_form_validator.resetForm();
    let booking_id = booking.booking_id;
    let payment_type_id = booking.payment_type_id;
    $('input[name="booking_id"]', retry_pay_form).val(booking_id);
    $('input[name="payment_method"]', retry_pay_form).val(payment_type_id);
    $('[class*=payment-type-]', retry_pay_form).hide();
    $('[id^=retry-btn-pay-mode-]', retry_pay_form).hide();
    $('.payment-type-' + payment_type_id, retry_pay_form).show();
    $('#retry-btn-pay-mode-' + payment_type_id, retry_pay_form).show();
    $('#retry-pay-popup').show(500);
}
function hideRetryPayPopup() {
    $('input[name="booking_id"]', retry_pay_form).val('');
    $('input[name="payment_method"]', retry_pay_form).val('');
    $('#retry-pay-popup').hide(500);
}
/******************************************************************************* */
// cancel schedule popup functions
$('#cancel-schedule-popup [data-action="close"]').click(function () {
    hideCancelPopup();
});
/******************************************************************************* */
function hidePayModePopup() {
    $('#change-pay-mode-popup').hide(500);
}
function checkoutTokenization(form) {
    submit_btn = $('button[type="submit"]', form);
    submit_btn.html(loading_button_html);
    /********************************* Tokenization ************************************/
    $.ajax({
        type: 'POST',
        url: _checkout_token_url,
        dataType: 'json',
        data: JSON.stringify({
            "type": "card",
            "requestSource": "JS",
            "number": $('input[name="card_number"]', form).val(),
            "expiry_month": moment($('input[name="exp_date"]', form).val(), "MM/YY").format("MM"),
            "expiry_year": moment($('input[name="exp_date"]', form).val(), "MM/YY").format("YYYY"),
            "cvv": $('input[name="cvv"]').val()
        }),
        // important
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _checkout_primary_key,
        },
        crossDomain: true,
        contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            _checkout_token_data = response;
            // submit booking form again
            $('input[name="checkout_token_data"]', form).val(JSON.stringify(response));
            form.submit();
        },
        error: function (response) {
            toast('Failed', 'Invalid card details', 'error');
            _checkout_token_data = undefined;
            submit_btn.html('Pay Now');
        },
    });
}