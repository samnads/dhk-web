$().ready(function () {
    invoice_form_validator = $('#online_payment_link_form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "customerId": {
                required: true,
            },
            "inv_id": {
                required: false,
            },
            "amount": {
                required: true,
            },
            "description": {
                required: true
            },
        },
        messages: {
            "customerId": {
                required: "Customer ID not found",
            },
            "inv_id": {
                required: "Invoice ID not found",
            },
            "amount": {
                required: "Enter amount",
            },
            "description": {
                required: "Enter description / message",
            },
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            form.submit();
        }
    });
});