// step button actions
$('[data-action="next-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    _last_step = false;
    if (isStepValid(current_step) === false) {
        // DONT'T GO TO NEXT STEP
        return;
    }
    let next_step = current_step + 1;
    //
    if (next_step == 2) {
        if ($('input[name="id"]', booking_form).val() == "") {
            // Case 1 : not logged in
            showLogin();
            return false;
        }
        else if ($('input[name="address_id"]', booking_form).val() == "") {
            // Case 2 : no default address id found (may be because of no addrees in db)
            showNewAddress();
            toast('Add Address', 'Please add address to continue.', 'info');
            return false;
        }
    }
    //
    $('.step-' + current_step).hide();
    $('.step-' + next_step).show();
    // do after going to step
    /************************************* */
    let _meta_title_new = _meta_title + ' ' + next_step + '/2';
    $(document).prop('title', _meta_title_new);
    /************************************* */
    if (next_step == 2) {
        $('input[name="payment_method"]:checked', booking_form).trigger('change');
        _last_step = true;
    }
    //
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', next_step)));
    window.scrollTo(0, 0);
});
$('[data-action="prev-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    let prev_step = current_step - 1;
    //
    $('.step-' + current_step).hide();
    $('.step-' + prev_step).show();
    _last_step = false;
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + prev_step + '/2');
    /************************************* */
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', prev_step)));
    window.scrollTo(0, 0);
});
$(booking_form).submit(function () {
    // just to show last step error toasts
    isStepValid(2);
});
function isStepValid(step) {
    var required = [];
    if (step == 1) {
        required = [
            {
                name: 'date',
                status: $('input[name="date"]', booking_form).valid()
            },
            {
                name: 'instructions',
                status: $('textarea[name="instructions"]', booking_form).valid()
            },
        ];
    }
    else if (step == 2) {
        required = [
            {
                name: 'payment_method',
                status: $('input[name="payment_method"]', booking_form).valid()
            },
            {
                name: 'accept_terms',
                status: $('input[name="accept_terms"]', booking_form).valid()
            },
        ];
    }
    /************************************************ */
    // check for errors and show as toast
    var errors = required.filter(field => {
        return field.status === false
    });
    if (errors.length > 0) {
        var first_error_field = errors[0].name;
        var error_message = booking_form_validator.submitted[first_error_field];
        toast(null, error_message, 'info');
        scrollToElement($('[name="' + first_error_field + '"]', booking_form));
        return false;
    }
    /************************************************ */
    // step valid
    return true;
}
/*********************************************************************************************/
function packageToggled() {
    _cart.packages = [];
    $('input[name="packages[]"]:checked', booking_form).each(function (index, obj) {
        var package = {
            package_id: $(this).val(),
            quantity: $('#package_quanity_' + $(this).val()).val(),
        }
        _cart.packages.push(package);
    });
    calculate();
}
$('input[name="packages[]"]', booking_form).change(function () {
    if (this.checked) {
        $('input[type="number"][data-package_id="' + this.value + '"]').val(1);
        $('#package-summary-row-' + this.value).show();
    }
    else {
        $('input[type="number"][data-package_id="' + this.value + '"]').val(0);
        $('#package-summary-row-' + this.value).hide();
    }
    packageToggled();
});
$('[data-action="package-plus"]', booking_form).click(function () {
    current_quantity = Number($('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val());
    max_quantity = $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').attr("max");
    if (current_quantity >= max_quantity) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(max_quantity);
        toast("Maximum selected", "Maximum of qty. " + max_quantity + " already selected.", 'info');
    }
    else {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(current_quantity + 1);
    }
    packageToggled();
});
$('[data-action="package-minus"]', booking_form).click(function () {
    current_quantity = Number($('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val());
    min_quantity = $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').attr("min");
    updated_quantity = current_quantity - 1;
    if (updated_quantity == 0) {
        $('#package-' + $(this).attr("data-package_id")).prop('checked', false);
        $('#package-summary-row-' + $(this).attr("data-package_id")).hide();
    }
    else if (updated_quantity > min_quantity) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(updated_quantity);
    }
    else {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(min_quantity);
    }
    packageToggled();
});
/*********************************************************************************************/
var available_datetime_req = null;
function available_datetimeRender() {
    $('#calendar-load').html(loading_html);
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_datetime_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_datetime",
        data: {
            service_type_id: $('input[name="service_type_id"]', booking_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_datetime_req != null) {
                available_datetime_req.abort();
            }
        },
        success: function (response) {
            $('#calendar-load').html('');
            var dates_html = ``;
            $.each(response.result.available_dates, function (index, date) {
                let disabled = response.result.disabled_dates.find(disabled_date => {
                    return disabled_date == date
                });
                disabled = disabled ? 'disabled' : '';
                title = disabled ? 'Holiday' : '';
                dates_html += `<div class="item">
                    <input id="date-`+ index + `" value="` + moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') + `" name="date" class="" type="radio" ` + disabled + `>
                    <label for="date-`+ index + `" title="` + title + `">
                        <div class="calendar-tmb-main">
                            <div class="calendar-tmb-day">`+ moment(date, 'YYYY-MM-DD').format('ddd') + `</div>
                            <div class="calendar-tmb-date">`+ moment(date, 'YYYY-MM-DD').format('DD') + `</div>
                            <div class="calendar-tmb-month">`+ moment(date, 'YYYY-MM-DD').format('MMM') + `</div>
                        </div>
                    </label>
                </div>`;
            });
            $('#calendar').html(dates_html);
            $('#booking-form input[name="date"]').change(function () {
                var summary_date = moment(this.value, 'DD/MM/YYYY').format('DD MMM YYYY');
                $('#booking-summary .date').html(summary_date);
                //available_timeRender();
                calculate();
            });
            $('#calendar.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
            $('#calendar.owl-carousel').find('.owl-stage-outer').children().unwrap();
            $('#calendar.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
            $("#calendar.owl-carousel").owlCarousel({
                nav: true,
                loop: false,
                dots: false,
                margin: 15,
                autoplay: false,
                autoplayTimeout: 2000,
                smartSpeed: 800,
                autoplayHoverPause: false,
                responsive: {
                    0: {
                        items: 5
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 10
                    },
                    1100: {
                        items: 10
                    },
                    1200: {
                        items: 10,
                        //margin: 50
                    }
                }
            }); //re-initialise the owl
            $('input[name="date"]:not([disabled]):first', booking_form).trigger('click');
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
/*********************************************************************************************/
function renderTimes(available_times) {
    var times_html = ``;
    $.each(available_times, function (index, time) {
        times_html += `<li>
                    <input id="time-`+ index + `" value="` + time + `" name="time" class="" type="radio">
                    <label for="time-`+ index + `">
                        <!--<p>AED 5 Extra</p>-->`+ moment(time, "HH:mmm").format("hh:mm A") + `
                    </label>
                </li>`;
    });
    $('#times-holder').html(times_html);
    $('#booking-form input[name="time"]').change(function () {
        var hours = $('#booking-form input[name="hours"]:checked').val();
        var summary_time_from = moment(this.value, 'H:mm').format('hh:mm A');
        var summary_time_to = moment(this.value, 'H:mm').add(hours, 'hours').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from + ` to ` + summary_time_to);
        calculate();
    });
    //$('#booking-form input[name="time"]:first').trigger('click');
}
/*********************************************************************************************/
var available_time_req = null;
function available_timeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        data: {
            date: $('input[name="date"]:checked', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
/*********************************************************************************************/
/*debug = true;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};*/
/*var merchantIdentifier = "merchant.com.emaid.elitmaidsUser.livennew";
if (window.ApplePaySession) {
    var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
    promise.then(function (canMakePayments) {
        if (canMakePayments) {
            // Display Apple Pay button here.
            logit('hi, I can do ApplePay');
        }
    });
} else {
    logit('ApplePay is not available on this browser');
}
/*********************************************************************************************/
$().ready(function () {
    $(".scroll").jScroll();
    available_datetimeRender();
    create_booking_req = null;
    booking_form_validator = $('#booking-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "date": {
                required: true,
            },
            "instructions": {
                required: false,
            },
            "payment_method": {
                required: true,
            },
            "accept_terms": {
                required: true,
            }
        },
        messages: {
            "date": {
                required: "Select service date",
            },
            "instructions": {
                required: "Type any instructions",
            },
            "payment_method": {
                required: "Select Payment Method",
            },
            "accept_terms": {
                required: "Accept Terms & Conditions",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "packages[]") {
            }
            else if (element.attr("name") == "date") {
                error.insertAfter($('#calendar').parent());
            }
            else if (element.attr("name") == "time") {
                error.insertAfter($('#times-holder').parent());
            }
            else if (element.attr("name") == "hours") {
                error.insertAfter($('#hours-count-holder').parent());
            }
            else if (element.attr("name") == "professionals_count") {
                error.insertAfter($('#professionals-count-holder').parent());
            }
            else if (element.attr("name") == "cleaning_materials") {
                error.insertAfter($('#cleaning-materials-holder').parent());
            }
            else if (element.attr("name") == "instructions") {
                error.insertAfter($('#booking-form textarea[name="instructions"]').parent());
            }
            else if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else if (element.attr("name") == "accept_terms") {
                error.insertAfter($('#booking-form input[name="accept_terms"]').parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            booking_form = form;
            createBooking();
            return false;
        }
    });
});
/*********************************************************************************************/
var session = null; // apple pay session store
function createBooking() {
    booking_form = $('#booking-form');
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    if (payment_method == 1) {
        // continue
    }
    else if (payment_method == 2) {
        // continue
    }
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html);
    booking_btn.attr('disabled', true);
    create_booking_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/create_booking",
        dataType: 'json',
        data: {
            address_id: $('input[name="address_id"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            date: $('input[name="date"]:checked', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            cleaning_materials: 0,
            payment_method: $('input[name="payment_method"]:checked', booking_form).val(),
            instructions: $('textarea[name="instructions"]', booking_form).val(),
            subscription_package_id: $('input[name="subscription_package_id"]', booking_form).val(),
            checkout_token_data: _checkout_token_data,
            weekdays: []
        },
        beforeSend: function () {
            if (create_booking_req != null) {
                create_booking_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (_calculation_data.input.payment_method == 1) {
                    booking_btn.attr('disabled', false);
                    booking_btn.html('Completed');
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
                else if (_calculation_data.input.payment_method == 2) {
                    // ccavenue
                    booking_btn.html('Please wait...');
                    $('[name="order_id"]', ccavenue_form).val(response.result.billing.order_id);
                    $('[name="amount"]', ccavenue_form).val(response.result.billing.amount);
                    $('[name="billing_name"]', ccavenue_form).val(response.result.billing.name);
                    $('[name="billing_address"]', ccavenue_form).val(response.result.billing.address);
                    $('[name="billing_city"]', ccavenue_form).val(response.result.billing.city);
                    $('[name="billing_state"]', ccavenue_form).val(response.result.billing.state);
                    $('[name="billing_zip"]', ccavenue_form).val(response.result.billing.zip);
                    $('[name="billing_country"]', ccavenue_form).val(response.result.billing.country);
                    $('[name="billing_tel"]', ccavenue_form).val(response.result.billing.tel);
                    $('[name="billing_email"]', ccavenue_form).val(response.result.billing.email);
                    ccavenue_form.submit();
                }
            }
            else {
                // create booking failed
                booking_btn.attr('disabled', false);
                booking_btn.html('Complete');
                create_booking_error(response);
            }
        },
        error: function (response) {
            ajaxError(null, response);
            //booking_btn.attr('disabled', false);
        },
    });
}