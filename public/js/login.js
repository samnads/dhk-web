
let required_popup = $('#required-popup');
$().ready(function () {
    $('input[name="otp"]').on('input', function (e) {
        let otp = this.value;
        if (otp.length == 4) {
            login_otp_popup_form.find('button[type="submit"]').focus();
            login_otp_popup_form.submit();
        }
    });
    $('[data-action="login-popup"]').click(function () {
        showLogin();
    });
    $('#login-popup [data-action="close"]').click(function () {
        hideLogin();
    });
    $('#otp-popup [data-action="close"]').click(function () {
        //$("#login-otp-popup-form input[name=otp]").val('');
        $('#otp-popup').hide(500);
    });
    login_form_validator = $('#login-popup-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "country_code": {
                required: true,
            },
            "mobilenumber": {
                required: 'input[name="type"][value="mobile"]:checked',
                minlength: 9,
                maxlength: 9
            },
            "email": {
                required: 'input[name="type"][value="email"]:checked',
            },
        },
        messages: {
            "country_code": {
                required: "Select your country",
            },
            "mobilenumber": {
                required: "Enter mobile number",
            },
            "email": {
                required: "Enter email address",
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/customer_login",
                dataType: 'json',
                //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
                data: $('#login-popup-form').serialize(),
                //contentType: 'application/json;charset=UTF-8',
                success: function (response) {
                    if (response.result.status == "success") {
                        toast('OTP Sent', 'Enter the OTP to continue !', 'info');
                        $('#login-otp-popup-form input[name="id"]').val(response.result.UserDetails.id);
                        $('#login-otp-popup-form input[name="mobilenumber"]').val(response.result.UserDetails.mobile);
                        $('#login-otp-popup-form input[name="oldmobilenumber"]').val(response.result.UserDetails.mobile);
                        let login_type = $('input[name="type"]:checked', login_popup_form).val();
                        if (login_type == 'mobile') {
                            $('.customer-full-mobile').html(response.result.UserDetails.mobile);
                        }
                        else if (login_type == 'email') {
                            $('.customer-full-mobile').html(response.result.UserDetails.email);
                            $('[name="email"]', login_otp_popup_form).val(response.result.UserDetails.email);
                        }
                        hideLogin();
                        showOtp();
                    }
                    else {
                        toast('Error', response.result.message, 'error');
                        submit_btn.html('Continue').prop("disabled", false);
                    }
                },
                error: function (response) {
                    submit_btn.html('Continue').prop("disabled", false);
                },
            });
        }
    });
    login_otp_form_validator = $('#login-otp-popup-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "country_code": {
                required: true,
            },
            "mobilenumber": {
                required: 'input[name="type"][value="mobile"]:checked',
            },
            "otp": {
                required: true,
            }
        },
        messages: {
            "country_code": {
                required: "Select your country",
            },
            "mobilenumber": {
                required: "Enter mobile number",
            },
            "otp": {
                required: "Enter the OTP",
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/check_otp",
                data: $(form).serialize(),
                success: function (response) {
                    if (response.result.status == "success") {
                        toast('Success', response.result.message, 'success');
                        submit_btn.html('Continue').prop("disabled", false);
                        $('.before-login').hide();
                        $('.after-login').show();
                        _id = response.result.UserDetails.id;
                        _token = response.result.UserDetails.token;
                        _email = response.result.UserDetails.email;
                        _mobile = response.result.UserDetails.mobile;
                        $('input[name="id"]').val(response.result.UserDetails.id);
                        $('input[name="token"]').val(response.result.UserDetails.token);
                        $('input[name="address_id"]').val(response.result.UserDetails.default_address_id);
                        if ($('#booking-form input[name="service_type_id"]').val() > 0) {
                            // it means someone is in the middle of the booking form (need to preserve the data) :(
                            calculate();
                            hideOtp();
                            if (response.result.UserDetails.UserName == null || response.result.UserDetails.email == null) {
                                showNameEntry();
                            }
                        }
                        else {
                            // may be not a service based form page (dont care about data)
                            hideOtp();
                            if (response.result.UserDetails.UserName == null || response.result.UserDetails.email == null) {
                                showNameEntry();
                            }
                            else {
                                fetchAddressList();
                            }
                        }
                    }
                    else {
                        submit_btn.html('Continue').prop("disabled", false);
                        toast('Incorrect OTP', response.result.message, 'warning');
                    }
                },
                error: function (response) {
                },
            });
        }
    });
    //$('#required-popup').show();
    if (_id != null && (_name == null || _name == "")) {
        showNameEntry();
    }
    required_form_validator = $('#required-popup-form').validate({
        focusInvalid: false,
        //ignore: [],
        rules: {
            "name": {
                required: true,
            },
            "email": {
                required: true,
            },
            "country_code": {
                required: true,
            },
            "mobilenumber": {
                required: true,
            },
        },
        messages: {
            "name": {
                required: "Enter your name",
            },
            "email": {
                required: "Enter your email address",
            },
            "mobilenumber": {
                required: "Enter your mobile number",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "mobilenumber") {
                error.insertAfter($('[name="mobilenumber"]').parent().parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/name_entry",
                data: {
                    name: $('input[name="name"]', form).val(),
                    email: $('input[name="email"]', form).val(),
                    country_code: $('input[name="country_code"]', form).val(),
                    mobilenumber: $('input[name="mobilenumber"]', form).val(),
                },
                success: function (response) {
                    if (response.result.status == "success") {
                        _id = response.result.UserDetails.id;
                        _token = response.result.UserDetails.token;
                        _name = response.result.UserDetails.UserName;
                        hideRequiredEntry();
                        toast('Saved', response.result.message, 'success');
                    }
                    else {
                        toast('Error', response.result.message, 'error');
                        submit_btn.html('Continue').prop("disabled", false);
                    }
                },
                error: function (response) {
                    submit_btn.html('Continue').prop("disabled", false);
                },
            });
        }
    });
});
const phone_input = document.querySelector('#required-popup [name="mobilenumber"]');
window.intlTelInput(phone_input, ({
    containerClass: "w-100",
    formatAsYouType: false,
    initialCountry: 'ae',
    placeholderNumberType: "polite",
    validationNumberType: "FIXED_LINE",
    strictMode: true,
    separateDialCode: false,
    utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@24.3.6/build/js/utils.js"
}));
const iti = intlTelInput(phone_input);
iti.setCountry("ae");
phone_input.addEventListener("countrychange", function () {
    $('[name="country_code"]', required_popup).val(iti.getSelectedCountryData().dialCode);
});


function showNameEntry() {
    $('#required-popup').show(500);
    if (_email != '' && _email != null) {
        // hide email prompt
        $('div.email', required_popup).hide();
    }
    else {
        // hide email prompt
        $('div.email', required_popup).show();
    }
    //
    if (_mobile != '' && _mobile != null) {
        // hide mobile prompt
        $('div.mobile', required_popup).hide();
    }
    else {
        // hide mobile prompt
        $('div.mobile', required_popup).show();
    }
    toast('Required', "Please enter the details to continue", 'info');
}
function hideRequiredEntry() {
    $('#required-popup').hide(500);
}
$('input[name="type"]', login_popup_form).change(function () {
    let login_type = this.value;
    if (login_type == 'mobile') {
        $('.login-field.email', login_popup_form).hide();
        $('.login-field.mobile', login_popup_form).show();
    }
    else if (login_type == 'email') {
        $('.login-field.mobile', login_popup_form).hide();
        $('.login-field.email', login_popup_form).show();
    }
});