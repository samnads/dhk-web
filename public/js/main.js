var _cart = {};
var _coupons_applied = [];
var _address_list = [];
var booking_form = $('#booking-form');
var _meta_title = $('title').text();
var _checkout_token_data = undefined;
var loading_html = `<div class="d-flex justify-content-center">
  <div class="spinner-border" role="status">
    <span class="visually-hidden">Loading...</span>
  </div>
</div>`;
var loading_button_html = `<span class="spinner-border spinner-border-sm" aria-hidden="true"></span>
  <span class="visually-hidden" role="status">Loading...</span>&nbsp;`;
let login_popup_form = $('#login-popup-form');
let login_otp_popup_form = $('#login-otp-popup-form');
let _address_from = $('#new-address-popup-form');
let cent_latitude = 25.055277;
let cent_longitude = 55.1586003;
var event_ = null;
let _last_step = false;
/************************************************************************************************* */
$('[data-action="service-select"]').click(function () {
    let service_type_model_id = this.getAttribute('data-service_type_model_id');
    let web_url_slug = this.getAttribute('data-web_url_slug');
    Cookies.remove('coupon_code');
    if (service_type_model_id == 1) {
        // normal service
        window.open(_base_url + web_url_slug, "_self");
    } else if (service_type_model_id == 2) {
        // package service
        window.open(_base_url + web_url_slug, "_self");
    }
    else if (service_type_model_id == 3) {
        // enquiry based
        window.open(_base_url + web_url_slug, "_self");
    }
    else {
        alert('Unknown Service Model !');
    }
});
/************************************************************************************************* */
$('[data-action="banner-select"]').click(function () {
    let service_type_model_id = this.getAttribute('data-service_type_model_id');
    let web_url_slug = this.getAttribute('data-web_url_slug');
    let package_id = Number(this.getAttribute('data-package_id'));
    let offer_id = Number(this.getAttribute('data-offer_id'));
    let coupon_code = this.getAttribute('data-coupon_code');
    if (offer_id > 0) {
        Cookies.set('coupon_code', coupon_code); // set offer coupon
        window.open(_base_url + web_url_slug + "/" + coupon_code, "_self"); // temp fix
    }
    else if (package_id > 0) {
        window.open(_base_url + 'package/' + package_id, "_self");
    }
});
/************************************************************************************************* */
$('[data-action="coupon-apply-popup"]').click(function () {
    showCouponPopup();
});
$('#coupon-apply-popup [data-action="close"]').click(function () {
    hideCouponPopup();
});
$('[data-action="remove-coupon"]').click(function () {
    _cart.coupon_code = null;
    $('#applied-coupon-widget').hide(500);
    $('#add-coupon-widget').show(500);
    toast('Coupon removed', "Applied coupon removed!", 'info');
    calculate();
});
function showCouponPopup() {
    coupon_apply_form.trigger("reset");
    coupon_form_validator.resetForm();
    $('#coupon-apply-popup').show(500);
}
function hideCouponPopup() {
    $('#coupon-apply-popup').hide(500);
}
/************************************************************************************************* */
calculate_req = null;
function calculate() {
    var booking_form = $('#booking-form');
    let coupon_apply_btn = $('#coupon-apply-form button[type="submit"]');
    //
    $('button[data-action="next-step"]').prop("disabled", true).html(loading_button_html);
    if (_last_step == true) {
        $('button[type="submit"]', booking_form).prop("disabled", true).html(loading_button_html);
    }
    //
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    calculate_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/calculate",
        dataType: 'json',
        data: {
            //id: $('input[name="id"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            hours: $('input[name="hours"]:checked', booking_form).val(),
            professionals_count: $('input[name="professionals_count"]:checked', booking_form).val(),
            addons: _cart.addons,
            packages: _cart.packages,
            frequency: $('input[name="frequency"]:checked', booking_form).val(),
            coupon_code: _cart.coupon_code,
            date: $('input[name="date"]:checked', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            payment_method: payment_method,
            subscription_package_id: $('input[name="subscription_package_id"]', booking_form).val(),
            //
            cleaning_materials: $('input[name="cleaning_materials"]:checked', booking_form).val() == 1 ? true : undefined,
            // DHK
            supervisor: $('input[name="supervisor"]:checked', booking_form).val(),
            cleaning_material_id: $('input[name="material_ids"]:checked', booking_form).val(),
            tool_ids: _cart.tool_ids,
            details: _cart.details,
            extra_services: _cart.extra_services,
            weekdays: _cart.weekdays,
            recurring_months: $('input[name="recurring_months"]:checked', booking_form).val()
        },
        beforeSend: function () {
            if (calculate_req != null) {
                calculate_req.abort();
            }
        },
        success: function (response) {
            _calculation_data = response.result.calculation_data;
            coupon_apply_btn.html('Apply').prop("disabled", false);
            if (_calculation_data.coupons_applied.length > 0) {
                // some coupon messages found !
                // show toast based on previous applied coupon data
                /*if (_coupons_applied.length == 0) {
                    // no history on current sessiion
                    if (_calculation_data.coupons_applied[0].status == "success") {
                        $('#applied-coupon-widget').show(500);
                        $('#add-coupon-widget').hide(500);
                        hideCouponPopup();
                        $('.coupon-code').html(_calculation_data.coupons_applied[0].coupon_code);
                        toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'success');
                    }
                    else {
                        $('#applied-coupon-widget').hide(500);
                        $('#add-coupon-widget').show(500);
                        toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'error');
                    }
                }
                else {
                    // check with prev coupon
                    var same_coupon = _calculation_data.coupons_applied.find(coupon => {
                        return coupon.coupon_code == _coupons_applied[0].coupon_code
                    });
                    if (same_coupon) {
                        // same coupon code found with history; do nothing
                        // show on error only
                        if (_calculation_data.coupons_applied[0].status != "success") {
                            $('#applied-coupon-widget').hide(500);
                            $('#add-coupon-widget').show(500);
                            //toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'error');
                        }
                        else if (_calculation_data.coupons_applied[0].status == "success") {
                            $('#applied-coupon-widget').show(500);
                            $('#add-coupon-widget').hide(500);
                            hideCouponPopup();
                            $('.coupon-code').html(_calculation_data.coupons_applied[0].coupon_code);
                            //toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'success');
                        }
                    }
                    else {
                        if (_calculation_data.coupons_applied[0].status == "success") {
                            $('#applied-coupon-widget').show(500);
                            $('#add-coupon-widget').hide(500);
                            hideCouponPopup();
                            $('.coupon-code').html(_calculation_data.coupons_applied[0].coupon_code);
                            toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'success');
                        }
                        else {
                            $('#applied-coupon-widget').hide(500);
                            $('#add-coupon-widget').show(500);
                            toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'error');
                        }
                    }

                }*/
                /********************************************************************* */
                // SHOW COUPON TOAST
                if (_coupons_applied.length == 0) {
                    // first toast
                    if (_calculation_data.coupons_applied[0].status == "success") {
                        $('#applied-coupon-widget').show(500);
                        $('#add-coupon-widget').hide(500);
                        $('.coupon-code').html(_calculation_data.coupons_applied[0].coupon_code);
                        toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'success');
                    }
                    else {
                        $('#applied-coupon-widget').hide(500);
                        $('#add-coupon-widget').show(500);;
                        toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'error');
                    }
                }
                else if (_coupons_applied[0].status != _calculation_data.coupons_applied[0].status) {
                    // coupon status not same as previous, so show toast
                    if (_calculation_data.coupons_applied[0].status == "success") {
                        $('#applied-coupon-widget').show(500);
                        $('#add-coupon-widget').hide(500);
                        $('.coupon-code').html(_calculation_data.coupons_applied[0].coupon_code);
                        toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'success');
                    }
                    else {
                        $('#applied-coupon-widget').hide(500);
                        $('#add-coupon-widget').show(500);
                        toast(_calculation_data.coupons_applied[0].title, _calculation_data.coupons_applied[0].message, 'error');
                    }
                }
                hideCouponPopup();
                /********************************************************************* */
            }
            _coupons_applied = _calculation_data.coupons_applied;
            $('calc-amount.service_amount_before_discount').html(Number(_calculation_data.summary.service_amount_before_discount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.service_discount').html(Number(_calculation_data.summary.service_amount_discount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.service_amount').html(Number(_calculation_data.summary.service_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.supervisor_charge').html(Number(_calculation_data.summary.supervisor_charge).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.cleaning_materials_amount').html(Number(_calculation_data.summary.cleaning_materials_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.tools_amount').html(Number(_calculation_data.summary.tools_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.discount_total').html(Number(_calculation_data.summary.discount_total).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.taxable_amount').html(Number(_calculation_data.summary.taxable_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.vat_amount').html(Number(_calculation_data.summary.vat_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.taxed_amount').html(Number(_calculation_data.summary.taxed_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.payment_type_charge').html(Number(_calculation_data.summary.payment_type_charge).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.total_payable').html(Number(_calculation_data.summary.total_payable).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            /****************************************************************** */
            if (_calculation_data.summary.service_amount_discount > 0) {
                $('#booking-summary .service_discount').show();
                $('#booking-summary .service_amount').show();
            }
            else {
                $('#booking-summary .service_discount').hide();
                $('#booking-summary .service_amount').hide();
            }
            if (_calculation_data.summary.discount_total > 0) {
                $('#booking-summary .discount_total').show();
            }
            else {
                $('#booking-summary .discount_total').hide();
            }
            if (_calculation_data.summary.payment_type_charge > 0) {
                $('#booking-summary .payment_type_charge').show();
            }
            else {
                $('#booking-summary .payment_type_charge').hide();
            }
            if (_calculation_data.summary.supervisor_charge > 0) {
                $('#booking-summary .supervisor_charge').show();
            }
            else {
                $('#booking-summary .supervisor_charge').hide();
            }
            if (_calculation_data.summary.cleaning_materials_amount > 0) {
                $('#booking-summary .cleaning_materials_amount').show();
            }
            else {
                $('#booking-summary .cleaning_materials_amount').hide();
            }
            if (_calculation_data.summary.tools_amount > 0) {
                $('#booking-summary .tools_amount').show();
            }
            else {
                $('#booking-summary .tools_amount').hide();
            }
            $('button[data-action="next-step"]').prop("disabled", false).html('Next');
            if (_last_step == true) {
                $('button[type="submit"]', booking_form).prop("disabled", false);
                if (payment_method == 1) {
                    // cash
                    $('button[type="submit"]', booking_form).html('Complete');
                }
                else if (payment_method == 2) {
                    // card
                    $('button[type="submit"]', booking_form).html('Pay Now');
                }
            }
        },
        error: function (response) {
            coupon_apply_btn.html('Apply').prop("disabled", false);
            calculate();
        },
    });
}
function toast(title = null, text = null, icon = "info", settings = {}) {
    $.toast({
        heading: title,
        text: text,
        showHideTransition: 'fade',
        position: 'top-center',
        icon: icon,
        stack: 1,
        hideAfter: 3000,
        allowToastClose: false
    })
}
$('[data-action="logout"').click(function () {
    $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/customer_logout",
        //dataType: 'json',
        //data: JSON.stringify($('#login-otp-popup-form').serializeObjectForApi()),
        //contentType: 'application/json;charset=UTF-8',
        data: {
            id: $('#id').val(),
            token: $('#token').val()
        },
        success: function (response) {
            window.location.href = _base_url;
        },
        error: function (response) {
            //ajaxError(null, response);
        },
    });
});
/************************************************************* */
$('#alert-popup [data-action="close"]').click(function () {
    hideTextAlert();
});
function showTextAlert(text = '') {
    $('#alert-popup p.text').text(text);
    $('#alert-popup').show(500);
}
function hideTextAlert(text) {
    $('#alert-popup').hide(500);
}
/************************************************************* */
function showLogin() {
    login_popup_form = $('#login-popup-form');
    login_popup_form.trigger("reset");
    login_form_validator.resetForm();
    let submit_btn = $('button[type="submit"]', login_popup_form);
    submit_btn.html('Continue').prop("disabled", false);
    $('input[name="type"]:checked', login_popup_form).trigger('change');
    $('.mobile-dropdown').hide(500);
    $('#login-popup').show(500);
}
function hideLogin() {
    $('#login-popup').hide(500);
}
/************************************************************* */
function showOtp(login_form = undefined) {
    startOTPTimer();
    login_otp_popup_form = $('#login-otp-popup-form');
    login_otp_popup_form.trigger("reset");
    login_otp_form_validator.resetForm();
    let submit_btn = $('button[type="submit"]', login_otp_popup_form);
    submit_btn.html('Continue').prop("disabled", false);
    //
    let login_type = $('input[name="type"]:checked', login_popup_form).val();
    $('[name="type"]', login_otp_popup_form).val(login_type);
    if (login_type == 'mobile') {
        $('h4', login_otp_popup_form).html('Verify phone number');
    }
    else if (login_type == 'email') {
        $('h4', login_otp_popup_form).html('Verify email address');
    }
    //
    $('#otp-popup').show(500);
    $('input[name="otp-1"]', login_otp_popup_form).focus();
}
function hideOtp() {
    $('#otp-popup').hide(500);
}
function locationPickr(latitude, longitude) {
    $('.us3').locationpicker({
        location: {
            latitude: latitude,
            longitude: longitude
        },

        radius: 0,
        inputBinding: {
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
            radiusInput: $('.us3-radius'),
            locationNameInput: $('.us3-address')
        },
        //markerIcon: _base_url +'images/picker.png',
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    });
}
function showPosition(position) {
    if (event_) {
        console.log(event_);
        event_.type = 'change';
        $('input[name="latitude"]', _address_from).val(position.coords.latitude).trigger(event_);
        $('input[name="longitude"]', _address_from).val(position.coords.longitude).trigger(event_);
    }
    else {
        $('#latitude').val(position.coords.latitude);
        $('#longitude').val(position.coords.longitude);
    }
    locationPickr(position.coords.latitude, position.coords.longitude);
}
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            $('.us3').locationpicker({
                location: {
                    latitude: cent_latitude,
                    longitude: cent_longitude
                },
                radius: 0,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    radiusInput: $('.us3-radius'),
                    locationNameInput: $('.us3-address')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.");
            break;
    }
}
$(document).ready(function () {
    if ($('#latitude').val() == '' || $('#longitude').val() == '') {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    } else {
        locationPickr($('#latitude').val(), $('#longitude').val());
    }
});
/************************************************************* */
$('[data-action="new-address-popup"]').click(function () {
    showNewAddress();
});
$('#new-address-popup [data-action="close"]').click(function () {
    hideNewAddress();
});
function showNewAddress(event) {
    event_ = event;
    address_form_validator.resetForm(); // remove errors
    $(_address_from).trigger("reset"); // reset fields
    $('input[name="address_id"]', _address_from).val(''); // for new purpose
    $('select[name="area_id"]', _address_from).selectpicker('val', '');
    $('#new-address-popup h4').html('New Address');
    $('button[type="submit"]', _address_from).html("Save Address");
    $('#new-address-popup').show(500);
    navigator.geolocation.getCurrentPosition(showPosition, showError);
}
function showEditAddress(address_id, event) {
    event_ = event;
    let address = _address_list.find(address => {
        return address.address_id == address_id
    });
    address_form_validator.resetForm(); // remove errors
    $(_address_from).trigger("reset"); // reset fields
    $('input[name="address_id"]', _address_from).val(address.address_id); // for edit purpose
    $('input[name="address_type"][value="' + address.address_type + '"]', _address_from).prop('checked', true);
    $('input[name="street"]', _address_from).val(address.street);
    $('input[name="building"]', _address_from).val(address.building);
    $('input[name="flat_no"]', _address_from).val(address.flat_no);
    $('select[name="area_id"]', _address_from).selectpicker('val', address.area_id);
    event.type = 'change';
    $('input[name="latitude"]', _address_from).val(address.lat).trigger(event);
    $('input[name="longitude"]', _address_from).val(address.long).trigger(event);
    $('#new-address-popup h4').html('Edit Address');
    $('button[type="submit"]', _address_from).html("Update Address");
    $('#new-address-popup').show(500);
}
function hideNewAddress() {
    $('#new-address-popup').hide(500);
}
/************************************************************* */
function fetchAddressList(render_on) {
    $('#profile-address-list-holder').html(loading_html);
    $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/address_list",
        dataType: 'json',
        success: function (response) {
            if (response.result.status == "success") {
                _address_list = response.result.address_list;
                let default_address = _address_list.find(address => {
                    return address.default_address == 1
                });
                renderAddressList(_address_list, render_on);
                if (default_address) {
                    if (booking_form) {
                        $('input[name="address_id"]', booking_form).val(default_address.address_id); // set address on booking form
                    }
                }
                else {
                    if (booking_form) {
                        $('input[name="address_id"]', booking_form).val(''); // set empty on booking form
                    }
                    showNewAddress();
                    toast('Add Address', 'Please add address to continue.', 'info');
                }
            }
            else {

            }
        },
        error: function (response) {

        },
    });
}
/************************************************************* */
function showAddressListPopup() {
    $('#address-list-popup').show(500);
}
function hideAddressListPopup() {
    $('#address-list-popup').hide(500);
}
/************************************************************* */
function renderAddressList(address_list, render_on) {
    address_html = ``;
    let addresses_list_html = ``;
    if (address_list.length == 0) {
        $('#profile-address-list-holder').html('No address found, please add atleast one address to start booking.');
    }
    else {
        $.each(address_list, function (index, address) {
            address_html += `<div class="col-sm-12 manage-address-section">
								<div class="d-flex manage-address-main">
									 <div class="manage-address-content flex-grow-1">
									      <input id="address-`+ index + `" value="` + address.address_id + `" name="address_id" class="" type="radio" ` + (address.default_address == 1 ? 'checked' : '') + `>
                                          <label for="address-`+ index + `">
										  
										  <div class="manage-address-edit-left">
											  <span></span>
											  <p><i class="fa fa-building-o text-info" aria-hidden="true"></i>&nbsp;&nbsp;` + address.flat_no + `, ` + address.building + ` [` + address.address_type_name + `]` + `</p>
												 <strong><i class="fa fa-map-marker text-info" aria-hidden="true"></i>&nbsp;&nbsp;`+ address.street + `, ` + address.area_name + `</strong>
										  </div>
										  
										  <div class="manage-address-edit v-center">
									      <div class="manage-address-edit-icons">
										  	   <label class="delete-action" data-action="delete-address-popup" data-id="` + address.address_id + `" data-default_address="` + address.default_address + `">` + (address.default_address == 1 ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-trash"></i>') + `</label>
										       <label class="show-add-address-popup" data-action="new-address-popup" data-id="` + address.address_id + `"><i class="fa fa-pencil"></i></label>
										  </div>
									 </div>
										  </label>
									 </div>
								</div>
						   </div>`;
            addresses_list_html += `<div class="col-sm-12">
								<div class="d-flex">
									 <div class="manage-address-content flex-grow-1">
									      <input id="address-list-`+ index + `" value="` + address.address_id + `" name="address_id" class="" type="radio" ` + (address.default_address == 1 ? 'checked' : '') + `>
                                          <label for="address-list-`+ index + `">
										  
										  <div class="manage-address-edit-left">
											  <span></span>
											  <p>` + address.address_type_name + `</p>
												 <strong>` + address.flat_no + `, ` + address.building + `, ` + address.street + `</strong>
										  </div>
										  </label>
									 </div>
								</div>
						   </div>`;
        });
        address_html += `<div class="col-sm-12 booking-main-btn-section pt-4">
        <div class="row m-0">
            <div class="col-lg-5 col-md-4 col-sm-6 booking-main-btn ps-0">
                <button class="text-field-btn" type="submit">Set Default Address</button>
            </div>
        </div>
    </div>`;
        $('#profile-address-list-holder').html(address_html);
        $('#addresses-list').html(addresses_list_html);
    }
    if ($('input[name="service_type_id"]', booking_form).val() > 0 && address_list.length > 1) {
        //showAddressListPopup();
    }
    $('[data-action="new-address-popup"]').click(function (e) {
        if ($(this).attr('data-id')) {
            // show for edit purpose only
            let address = _address_list.find(address => {
                return address.address_id == $(this).attr('data-id')
            });
            showEditAddress($(this).attr('data-id'), e);
        }
    });
    $('[data-action="delete-address-popup"]').click(function () {
        if ($(this).attr('data-default_address') == 1) {
            toast('Can\'t Delete', 'Default address can\'t deleted.', 'warning');
        }
        else {
            let address_id = this.getAttribute('data-id');
            Swal.fire({
                title: "Confirm delete?",
                text: "Are you sure want to delete this address?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "lightgreen",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                focusCancel: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: _base_url + "api/customer/delete_address",
                        data: {
                            address_id: address_id,
                        },
                        success: function (response) {
                            if (response.result.status == "success") {
                                fetchAddressList();
                                toast('Deleted', response.result.message, 'success');
                            }
                            else {
                                toast('Error', response.result.message, 'error');
                            }
                        },
                        error: function (response) {
                        },
                    });
                }
            });
        }
    });
    _address_from = $('#new-address-popup-form');
}
/************************************************************* */
function scrollToElement(element) {
    if ($(element).is(":visible")) {
        $("html").animate(
            {
                scrollTop: element.offset().top - 100
            },
            800 //speed
        );
    }
    else {
        // some element not visible so use parent
        $("html").animate(
            {
                scrollTop: element.parent().offset().top - 100
            },
            800 //speed
        );
    }
}
/************************************************************* */
function resetPayBtnName(payment_method) {
    if (payment_method == 1) {
        // cash
        return 'Complete';
    }
    else if (payment_method == 2) {
        // card
        return 'Pay Now';
    }
    else if (payment_method == 3) {
        // apple pay
        return 'Pay Now';
    }
}
/************************************************************* */
$().ready(function () {
    jQuery.fn.visible = function () {
        return this.css('visibility', 'visible');
    };
    jQuery.fn.invisible = function () {
        return this.css('visibility', 'hidden');
    };
    jQuery.fn.visibilityToggle = function () {
        return this.css('visibility', function (i, visibility) {
            return (visibility == 'visible') ? 'hidden' : 'visible';
        });
    };
    address_form_validator = $('#new-address-popup-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "address_type": {
                required: true,
            },
            "street": {
                required: true,
            },
            "building": {
                required: true,
            },
            "flat_no": {
                required: true,
            },
            "area_id": {
                required: true,
            }
        },
        messages: {
            "address_type": {
                required: "Select address type",
            },
            "street": {
                required: "Enter street name",
            },
            "building": {
                required: "Enter building name",
            },
            "flat_no": {
                required: "Enter Apt. / Villa / Office No.",
            },
            "area_id": {
                required: "Select your area",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "area_id") {
                error.insertAfter($('select[name="area_id"]').parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let address_id = $('input[name="address_id"]', form).val();
            let api_endpoint = "add_address";
            let submit_btn_text = "Save Address";
            if (address_id != "") {
                api_endpoint = "edit_address";
                submit_btn_text = "Update Address";
            }
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/" + api_endpoint,
                dataType: 'json',
                //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
                data: $('#new-address-popup-form').serialize(),
                //contentType: 'application/json;charset=UTF-8',
                success: function (response) {
                    submit_btn.html(submit_btn_text).prop("disabled", false);
                    if (response.result.status == "success") {
                        hideNewAddress();
                        fetchAddressList();
                        toast('Saved', response.result.message, 'success');
                    }
                    else {

                    }
                },
                error: function (response) {
                    submit_btn.html(submit_btn_text).prop("disabled", false);
                },
            });
        }
    });
    $('#new-address-popup-form select[name="area_id"]').change(function () {
        $(this).valid();
    });
    $('.user-btn').click(function () {
        $('.mobile-dropdown').toggle(500);
    });
    $('[data-action="resend-otp"').click(function () {
        let submit_btn = $('#login-otp-popup-form button[type="submit"]');
        submit_btn.html(loading_button_html).prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: _base_url + "api/customer/resend_otp",
            data: $('#login-popup-form').serialize() + '&id=' + encodeURIComponent($('input[name="id"]', login_otp_popup_form).val()),
            success: function (response) {
                //window.location.href = _base_url;
                if (response.result.status == "success") {
                    startOTPTimer();
                    submit_btn.html('Continue').prop("disabled", false);
                    toast('OTP Sent', response.result.message, 'info');
                    login_otp_popup_form.trigger("reset");
                    login_otp_form_validator.resetForm();
                }
                else {
                    submit_btn.html('Continue').prop("disabled", false);
                    toast('Error', response.result.message, 'error');
                }
            },
            error: function (response) {
                submit_btn.html('Continue').prop("disabled", false);
            },
        });
    });


    $(".tooltip-ation-main").click(function () {
        $('.tooltip-ation-main').removeClass('active');
        $(this).toggleClass('active');

    });



    $('.mob-total-left').click(function () {
        $('.booking-summary-section').toggle(500);
        $('.booking-main-btn-section').addClass('active');
    });



    $('.mob-booking-title').click(function () {
        $('.booking-summary-section').hide(500);
        $('.booking-main-btn-section').removeClass('active');
    });
});
$('[data-action="next-step"]').click(function () {
    // on next step click, hide if summary popup is showing
    if ($('.mob-booking-title').is(":visible")) {
        // mobile summary popup showing, just hide it
        $('.booking-summary-section').hide(500);
        $('.booking-main-btn-section').removeClass('active');
    }
});
function fillOTPfromBoxes() {
    $('input[name="otp"]').val($('input[name="otp-1"]').val() + $('input[name="otp-2"]').val() + $('input[name="otp-3"]').val() + $('input[name="otp-4"]').val());
}
function isOTPBoxesFilled() {
    let filled = true;
    $('input[name*="otp-"]').each(function (index, obj) {
        if (this.value == "") {
            filled = false;
        }
        else {
        }
    });
    return filled;
}
var otp_interval = null;
function startOTPTimer() {
    var counter = _otp_expire_seconds;
    $('#resend-otp-timer').text('Resend OTP in ' + moment.utc(_otp_expire_seconds * 1000).format('mm:ss'));
    $('#resend-otp-timer').show();
    $('#resend-otp').hide();
    clearInterval(otp_interval);
    otp_interval = setInterval(function () {
        counter--;
        // Display 'counter' wherever you want to display it.
        if (counter <= 0) {
            clearInterval(otp_interval);
            $('#resend-otp-timer').hide();
            $('#resend-otp').show();
            return;
        } else {
            $('#resend-otp-timer').text('Resend OTP in ' + moment.utc(counter * 1000).format('mm:ss'));
        }
    }, 1000);
    $('input[name="otp-1"]').focus();
}
$().ready(function () {
    $('input[name*="otp"]').keyup(function (e) {
        var key = e.keyCode || e.charCode;
        //e.preventDefault();
        let current_box = Number($(this).attr('data-box'));
        let input_length = $('input[name="otp-' + current_box + '"]').val().length;
        let current_box_value = $('input[name="otp-' + current_box + '"]').val();
        if (key == 8 || key == 46) {
            // deleting
            if (current_box_value == "") {
                $('input[name="otp-' + (current_box - 1) + '"]').focus().select();
            }
            else {
                $('input[name="otp-' + (current_box) + '"]').val('');
            }
        }
        else {
            if (input_length > 1) {
                $('input[name="otp-' + current_box + '"]').val(current_box_value.substr(current_box_value.length - 1));
                if (current_box <= 3) {
                    $('input[name="otp-' + (current_box + 1) + '"]').focus();
                }
            }
            else {
                if (current_box <= 3 && current_box_value != "") {
                    $('input[name="otp-' + (current_box + 1) + '"]').focus();
                }
            }
        }
        fillOTPfromBoxes();
        if (isOTPBoxesFilled() === true) {
            $('#login-otp-popup-form button[type="submit"]').focus();
            $('#login-otp-popup-form').submit();
        }
    });
    $('input[name*="otp"]').click(function () {
        this.focus(); //sets focus to element
        var val = this.value; //store the value of the element
        this.value = ''; //clear the value of the element
        this.value = val; //set that value back.
        this.select();
    });
});
function loader(show = true) {
    if (show == true) {
        $('#loader').show();
        $("body").addClass("overflow-hidden");
    }
    else {
        $('#loader').hide();
        $("body").removeClass("overflow-hidden");
    }
}
function google_maps_callback() { }
function google_pay_callback() { }